<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'gorgazon');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ':})7]U+lOI6P#oI.mBfeqSfqA]N5V|*~XZM$*i@*+y=Ej~u>!w,$4R9a.8~amNs#');
define('SECURE_AUTH_KEY',  ' }x}X}JC!,K=a3}BMp6X?{m>C*|rm_Z*^5W9*XB5nLWxw,GFh.W43ETl^l@TbF9A');
define('LOGGED_IN_KEY',    'Zt_; v@0C)A$BLYc]`oC/6UKM}FH`BrU!><m-sn[k5j(MwVQZMT*_?ksLkD-4AO!');
define('NONCE_KEY',        '~>2P0RAB~6`$JOUMOT &!R5~.Oyoz/Sy9s}nPEN=|2MvBHgu#xSI&G8R)2ZK7H8y');
define('AUTH_SALT',        '0ivVRC7n8zo|M0l.|44xPx-*2gO=[S~sKYEX6:{z=bjaKjw+5y,2e,CQ#9P <6N%');
define('SECURE_AUTH_SALT', '@sN)Q4-tKSjUlW8+LUcFl-jwmLwn2^T7SBnz{}/2d^N;r0g32~kl=jKBT.WzKysH');
define('LOGGED_IN_SALT',   '+f,%?!{}8HDH_Bld!-`Ywc(H7-~)(>g%6(YK! -hhCs_Y^T9|?&ZG?Sc{V~2W4uU');
define('NONCE_SALT',       'AfYB+]xJu<sDW^mmGpjFym,72fj|BBr96m{LJc}(d8CF{x[O<|L4L_>5Ao$E3+Yi');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
