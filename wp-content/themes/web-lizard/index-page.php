<?php
/**
 * Template Name: страница "index"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<div class="main-content">
			<div class="index-banner">
				<img src="<?=get_stylesheet_directory_uri()?>/images/other/index-banner.png">
			</div>

			<h2 class="h2-link">Продукция <a href="">перейти в каталог</a></h2>
			
			<div class="text-module--type-1">
				<p>У нас Вы можете купить <strong>рулонный газон</strong>, а также заказать устройство <strong>посевного газона</strong>. А еще мы предлагаем недорогую и оперативную <strong>доставку газона</strong> по европейской части России.</p>
			</div>
			<div class="services-list">
				<?
				for ($i=0; $i < 3; $i++) { ?>
				<div class="services-list--item">
					<div class="services-list--img" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/demo-1.png)"><a href=""></a></div>
					<div class="service-list--text">
						<h3>Пример продукции</h3>
						<p>Профессиональная укладка рулонного газона. 100% гарантия приживаемости. Срок выполнения работ от 1 дня.</p>
						<div class="show-more-link"><a href="">Подробнее</a></div>
					</div>
					<div class="service-list--price">
						от 49 руб./кв.м. <br><small>Цены указаны с НДС 20%</small>
					</div>
				</div>
				<?}?>
			</div>
			<div class="show-mobile all-list-link"><a href="">Все услуги</a></div>
			<div class="space"></div>

			<h2 class="h2-link">Услуги <a href="">все услуги</a></h2>

			<div class="services-list">
				<?
				for ($i=0; $i < 3; $i++) { ?>
				<div class="services-list--item">
					<div class="services-list--img" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/demo-1.png)"><a href=""></a></div>
					<div class="service-list--text">
						<h3>Пример услуги</h3>
						<p>Профессиональная укладка рулонного газона. 100% гарантия приживаемости. Срок выполнения работ от 1 дня.</p>
						<div class="show-more-link"><a href="">Подробнее</a></div>
					</div>
					<div class="service-list--price">
						от 49 руб./кв.м. <br><small>Цены указаны с НДС 20%</small>
					</div>
				</div>
				<?}?>
			</div>
			<div class="space"></div>

			<?include 'inc/realize-projects.php';?>
			<div class="space"></div>
			<div class="about-company">
				<div class="group">
					<div class="cell size-50 about-company--left">
						<h2>О компании </h2>
						<h3>2 250 000 м2</h3>
						<p>Площадь выращенного газонного покрытия высочайшего европейского качества.</p>
						<h3>150</h3>
						<p>Опытных сотрудников в штате</p>
						<h3>16 000</h3>
						<p>Завершонных проектов</p>
						<h3>16</h3>
						<p>Лет работы на рынке</p>
					</div>
					<div class="cell size-50 about-company--right">
						<img src="<?=get_stylesheet_directory_uri()?>/images/svg/abc-1.svg">
						<h3>Собственные производственные мощности</h3>
						<p>Специально обрабатываемые поля; спецтехника  по посеву, уходу и срезу готового материала; продуманная система орошения.</p>
						<img src="<?=get_stylesheet_directory_uri()?>/images/svg/abc-2.svg">
						<h3>Международные стандарты</h3>
						<p>Процесс производства газонов сертифицирован по международным стандартам ISO 9000 и ISO 14000.</p>
						<img src="<?=get_stylesheet_directory_uri()?>/images/svg/abc-3.svg">
						<h3>Передовые технологии производства</h3>
						<p>Делая упор на специализированном современном оборудовании, наша компания беспроигрышно опережает конкурентов.</p>
					</div>
				</div>
			</div>

			<?include "inc/trust-block.php"?>	

			<h2 class="h2-link">Отзывы о работе <a href="">все отзывы</a></h2>
			<div class="reviews">
				<div class="reviews--list reviews--slider">
					<?
					for ($i=0; $i < 3; $i++) { ?>
						<div class="item">
							<div class="reviews--item">
								<div class="reviews--avatar" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/avatar-demo.jpg)"></div>
								<div class="reviews--text-place">
									<div class="reviews--link">Отзыв о услуге: <a href="">Укладка рулонного газона на готовое основание</a></div>
									<div class="reviews-total">
										<div class="number-rating">4.5</div>
										<div class="stars"><img src="<?=get_stylesheet_directory_uri()?>/images/other/rating.png" alt=""></div>
									</div>
									<div class="reviews--people-info">
										<span>Игорь</span>
										<span>Организация</span>
									</div>
									<div class="reviews--review-text">Разнообразный и богатый опыт реализация намеченных плановых заданий способствует подготовки и реализации существенных финансовых и административных условий. Идейные соображения высшего порядка, а также постоянное информационно-пропагандистское обеспечение нашей деятельности позволяет оценить значение модели развития. Таким образом постоянный количественный рост и сфера нашей активности требуют определения и уточнения дальнейших направлений развития.</div>
								</div>
							</div>
						</div>
					<?}?>					
				</div>
			</div>

			<div class="show-mobile all-list-link"><a href="">Все отзывы</a></div>

			<div class="space"></div>

			<h2 class="h2-link">Блог <a href="">все статьи</a></h2>

			<div class="blog-list adapt-overflow">
				<?
				for ($i=0; $i < 2; $i++) { ?>
				<div class="blog-list-item">
					<div class="blog-list-item--image" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/blog-item.png)"></div>
					<div class="blog-list-item--place">
						<div class="rating"><img src="<?=get_stylesheet_directory_uri()?>/images/other/rating.png" alt=""></div>
						<div class="title">Правильная укладка рулонного газона</div>
						<p>Универсальный газон, подходит для широкого спектра озеленения</p>
						<ul class="blog-attributes">
							<li><a href="">Укладка</a></li>
							<li>12 декабря 2019</li>
							<li>Горгазон</li>
						</ul>
					</div>
				</div>
				<?}?>
			</div>
		</div>
	</div>
	<?include "inc/question-form.php"?>
	<?include "inc/article-determined-block.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>