$(document).ready(function() {

	$('select').styler();


	$(".js-open-sandwich").click(function(event) {
		$(".adapt-header .sandwich-open").slideToggle(108);
		blur(true);
	});
	$(".js-close-sandwich").click(function(event) {
		$(".adapt-header .sandwich-open").slideToggle(108);
		blur(false);
	});


	$(".js-open-adapt-search").click(function(event) {

		$(this).toggleClass('on');
		$(".adapt-search-open").toggle();

		if ($(this).hasClass('on')) {
			blur(true);
		} else {
			blur(false);
		}

		return false;
	});

	$(".js-open-adapt-share").click(function(event) {

		$(this).toggleClass('on');
		$(".adapt-share-open").toggle();

		if ($(this).hasClass('on')) {
			blur(true);
		} else {
			blur(false);
		}
		
		return false;
	});

	$(".js-open-share").click(function(event) {
		/* Act on the event */
		$(this).toggleClass('on');
		$(".share-open").toggle();
		
		return false;
	});

	$(".js-toggle-ul").click(function(event) {
		$(this).closest('.cell').find('ul').slideToggle(333);
	});

	// open modal windows

	$(".js-add-review").click(function(event) {
		blur(true);
		$(".modal-window--add-review").show();
		return false;
	});

	$(".js-add-review").click(function(event) {
		blur(true);
		$(".modal-window--add-review").show();
		return false;
	});

	$(".js-open-calc").click(function(event) {
		/* Act on the event */
		blur(true);
		$(".modal-window--calc").show();
		return false;		
	});

	$(".js-open-callback-form").click(function(event) {
		blur(true);
		$(".modal-window--callback").show();
		return false;
	});


	
	// end open modal windows

	$(".window-blur, .modal-window .close, .calc--close, .js-close-modal-windows").click(function(event) {
		$(".window-blur").hide();
		$(".adapt-header--buttons a").removeClass('on');
		$(".adapt-share-open, .modal-window, .adapt-search-open").hide();
	});

	$(".tab-switcher li").click(function(event) {
		$(".tab-switcher li").removeClass('on');
		$(this).addClass('on');
		var open = parseInt($(this).attr("data-open"));
		$(".hidden-sections .section").hide();
		$(".hidden-sections .section:nth-child("+open+")").slideDown(333);
	});

	$(".js-article-determined-block--open-seo a").click(function(event) {
		$(".article-determined-block--hidden").slideToggle(333);
		$(this).toggleClass('on');
		if ($(this).hasClass('on')) {
			$(this).html("Показать меньше");
		} else {
			$(this).html("Показать показать больше");			
		}
		return false;		
	});

	$(".js-open-comment-form").click(function(event) {
		$(".add-comment--open-form").slideDown(333);
		$(this).hide();
	});

	


	// dynamic-input

	$(".dynamic-input").click(function(event) {
		$(this).addClass('focus');
	});

	$(".dynamic-input input").blur(function(event) {
		if ($(this).val()) {
			$(this).closest('.dynamic-input').addClass('focus');
		} else {
			$(this).closest('.dynamic-input').removeClass('focus');			
		}
	});

	$(".faq-list--item").click(function(event) {
		$(this).toggleClass('open');
	});

	// dynamic-input end

	// module stars 

	$(".module-stars").each(function(index, el) {
		var rating = $(this).attr('data-rating');
		var percent_rating;
		switch (rating) {
			case "5":
				percent_rating = 100;
				break;
			case "4.5":
				percent_rating = 92;
				break;
			case "4":
				percent_rating = 80;
				break;
			case "3.5":
				percent_rating = 72;
				break;
			case "3":
				percent_rating = 60;
				break;
			case "2.5":
				percent_rating = 52;
				break;
			case "2":
				percent_rating = 40;
				break;
			case "1.5":
				percent_rating = 30;
				break;
			case "1":
				percent_rating = 20;
				break;
			case ".5":
				percent_rating = 10;
				break;
			case "0":
				percent_rating = 0;
				break;
		}
		$(this).find('.module-stars--control').css("width", percent_rating+"%");
	});

	$(".module-stars.active").mousemove(function(e) {
		var offset = $(this).offset();
		var relativeX = (e.pageX - offset.left);

		var total_width = $(this).width();
		var relativeX_percent = (relativeX / total_width) * 100;
		var current_percent = Math.round(relativeX_percent);
		var rating;

		if (current_percent >= 0) {
			rating = ".5";
		}
		if (current_percent >= 10) {
			rating = "1";
		}
		if (current_percent >= 20) {
			rating = "1.5";
		}
		if (current_percent >= 30) {
			rating = "2";
		}
		if (current_percent >= 40) {
			rating = "2.5";
		}
		if (current_percent >= 52) {
			rating = "3";
		}
		if (current_percent >= 60) {
			rating = "3.5";
		}
		if (current_percent >= 72) {
			rating = "4";
		}
		if (current_percent >= 80) {
			rating = "4.5";
		}
		if (current_percent >= 92) {
			rating = "5";
		}

		switch (rating) {
			case "5":
				percent_rating = 100;
				break;
			case "4.5":
				percent_rating = 92;
				break;
			case "4":
				percent_rating = 80;
				break;
			case "3.5":
				percent_rating = 72;
				break;
			case "3":
				percent_rating = 60;
				break;
			case "2.5":
				percent_rating = 52;
				break;
			case "2":
				percent_rating = 40;
				break;
			case "1.5":
				percent_rating = 30;
				break;
			case "1":
				percent_rating = 20;
				break;
			case ".5":
				percent_rating = 10;
				break;
			case "0":
				percent_rating = 0;
				break;
		}

		$(this).find('.module-stars--control').css("width", percent_rating+"%");
		$(this).find('.module-stars--control').attr("data-rating", rating);
	});

	// module stars end


	// calc 

	$(".js-open-adapt-calc-results").click(function(event) {
		$(".calc--right-side .gray-place").slideDown(333);
	});

	$(".js-close-adapt-calc-results").click(function(event) {
		$(".calc--right-side .gray-place").slideUp(333);
	});

	// calc end

	function blur($toggle) {
		if ($toggle) {
			$(".window-blur").show();
		} else {
			$(".window-blur").hide();
		}
	}
});