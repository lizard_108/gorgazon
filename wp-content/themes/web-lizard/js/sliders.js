$(document).ready(function() {

	var services_product__slider = $('.js-services-product--slider');


	services_product__slider.owlCarousel({
		navigation: true,
		pagination: false,
		slideSpeed: 444,
		paginationSpeed: 333,
		singleItem: true,
		touchDrag: true,
		mouseDrag: true,
		autoPlay: false,
		afterMove: callback,
		navigationText: false
	});

	var owl = services_product__slider.data('owlCarousel');

	$(".services-product--slider--subslider .item").click(function(event) {
		$(".services-product--slider--subslider .item").removeClass('current');
		$(this).addClass('current');

		owl.goTo($(this).index());
	});

	function callback(event) {
		var current = owl.currentItem + 1;
		$(".services-product--slider--subslider .item").removeClass('current');
		$(".services-product--slider--subslider .item:nth-child("+current+")").addClass('current');
	}

	sliders_init();
	$(window).resize(function(event) {
		sliders_init();
	});
});

function sliders_init() {
	if ($(window).width() >= 768) {
		$(".trust-block--slider").owlCarousel({
			navigation: true,
			pagination: false,
			slideSpeed: 444,
			paginationSpeed: 333,
			singleItem: false,
			touchDrag: true,
			mouseDrag: true,
			autoPlay: false,
			navigationText: false,
			items: 4,
			itemsDesktop : [1199,3],
			itemsDesktopSmall : [980,3],
			itemsTablet: [768,2],
			itemsTabletSmall: false,
			itemsMobile : [479,2],
		});
		$(".realize-projects--slider").owlCarousel({
			navigation: true,
			pagination: false,
			slideSpeed: 444,
			paginationSpeed: 333,
			singleItem: false,
			items: 2,
			itemsDesktop : [1199,2],
			itemsDesktopSmall : [980,2],
			itemsTablet: [768,1],
			itemsTabletSmall: false,
			itemsMobile : [479,1],

			touchDrag: true,
			mouseDrag: true,
			autoPlay: false,
			navigationText: false,
		});

		$(".reviews--slider").owlCarousel({
			navigation: true,
			pagination: false,
			slideSpeed: 444,
			paginationSpeed: 333,
			singleItem: true,
			touchDrag: true,
			mouseDrag: true,
			autoPlay: false,
			navigationText: false
		});
		
	} else {
		var slider_1 = $(".trust-block--slider").data("owlCarousel");
		var slider_2 = $(".realize-projects--slider").data("owlCarousel");
		var slider_3 = $(".reviews--slider").data("owlCarousel");

		if (
			slider_1 != undefined &&
			slider_2 != undefined &&
			slider_3 != undefined
		) {
			slider_1.destroy();
			slider_2.destroy();
			slider_3.destroy();
		}
	}
}