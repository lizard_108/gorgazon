<?php
/**
 * Template Name: страница "Вакансии"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	
		<div class="main-content">
			<h1>Вакансии</h1>
			<div class="maintext"><p>Компания Горгазон <strong>приглашает к сотрудничеству профессионалов</strong> своего дела, которые любят природу и готовы заниматься озеленением, укладкой и обслуживанием газонов вместе с нами!</p></div>
			<ul class="article-list">
				<li><a href="">Агроном (специалист по газонам)</a></li>
				<li><a href="">Садовник</a></li>
			</ul>
			<div class="space"></div>
			<div class="space"></div>
			<div class="avantgardes-block">
				<div class="group">
					<div class="cell size-50">
						<h3>Горгазон – молодая амбициозная команда специалистов</h3>
					</div>
					<div class="cell size-50">
						<p>Которые найдут подход даже к самому претенциозному клиенту! Мы приглашаем в свои ряды таких же неравнодушных к озеленению. Среди наших партнеров – строительные компанияи и девелоперы, студии ландшафтного дизайна и озеленители, садовые центры и прочие организации. Среди наших заказчиков: Правительство Москвы, Caddell Constraction Co,  Правительство Московской области, РПЦ, Сбербанк России, Аэропорт Домодедово, Аэропорт Шереметьево, стадион Открытие, Автодор и многие другие! Работать с нами  просто и, что важно, выгодно. Давайте гордиться нашими проектами вместе!</p>
					</div>
				</div>				
			</div>
		</div>
	</div>
	
	<?include "inc/question-form.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>