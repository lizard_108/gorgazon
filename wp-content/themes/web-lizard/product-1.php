<?php
/**
 * Template Name: страница "Продукция - 1"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	
		<div class="main-content">
			<h1>Продукция</h1>
			<div class="services-list">
				<?
				for ($i=0; $i < 3; $i++) { ?>
				<div class="services-list--item">
					<div class="services-list--img" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/demo-1.png)"><a href=""></a></div>
					<div class="service-list--text">
						<h3>Рулонный газон</h3>
						<p>Профессиональная укладка рулонного газона. 100% гарантия приживаемости. Срок выполнения работ от 1 дня.</p>
						<div class="show-more-link"><a href="">Подробнее</a></div>
					</div>
					<div class="service-list--price">
						от 49 руб./кв.м. <br><small>Цены указаны с НДС 20%</small>
					</div>
				</div>
				<?}?>	

				<?include "inc/trust-block.php"?>	
				<?include "inc/benefits-block.php"?>		
			</div>
		</div>
	</div>
	
	<?include "inc/question-form.php"?>
	<?include "inc/article-determined-block.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>