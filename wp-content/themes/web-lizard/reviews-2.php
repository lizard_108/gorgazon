<?php
/**
 * Template Name: страница "Отзывы - 2"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	
		<div class="space"></div>
		<div class="reviews">
			<div class="reviews--list">
				<div class="reviews--item">
					<div class="reviews--avatar" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/avatar-demo.jpg)"></div>
					<div class="reviews--text-place">
						<div class="reviews--link">Отзыв о услуге: <a href="">Укладка рулонного газона на готовое основание</a></div>
						<div class="reviews-total">
							<div class="number-rating">4.5</div>
							<div class="stars">
								<div class="module-stars size-small" data-rating="4.5">
									<div class="module-stars--control"></div>
								</div>
							</div>
						</div>
						<div class="reviews--people-info">
							<span>Игорь</span>
							<span>Организация</span>
						</div>
						<div class="reviews--review-text">Разнообразный и богатый опыт реализация намеченных плановых заданий способствует подготовки и реализации существенных финансовых и административных условий. Идейные соображения высшего порядка, а также постоянное информационно-пропагандистское обеспечение нашей деятельности позволяет оценить значение модели развития. Таким образом постоянный количественный рост и сфера нашей активности требуют определения и уточнения дальнейших направлений развития.</div>
						<div class="reviews--attach-list">
							<img src="<?=get_stylesheet_directory_uri()?>/images/other/reviews-attach.jpg">
							<img src="<?=get_stylesheet_directory_uri()?>/images/other/reviews-attach.jpg">
							<img src="<?=get_stylesheet_directory_uri()?>/images/other/reviews-attach.jpg">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="space"></div>
	</div>

	<?include "inc/question-form.php"?>
	<?include "inc/article-determined-block.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>