<?php
/**
 * Template Name: страница "Услуги - 4 - Отзывы о Укладка рулонного газона  на готовое основание"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	

		<div class="services-submenu">
			<div class="services-submenu--title">Укладка рулонного газона на готовое основание</div>
		</div>
		<ul class="tab-switcher">
			<li data-open="1">Описание</li>
			<li data-open="2">Преимущества</li>
			<li data-open="3">Стоимость газона</li>
			<li data-open="4" class="on">Отзывы</li>
			<li data-open="5">Выезд озеленителя</li>
		</ul>
		
		<div class="main-content">
			<h1>Отзывы о Укладка рулонного газона  на готовое основание </h1>
			<div class="review--product">
				<div class="review--product-image">
					<img src="<?=get_stylesheet_directory_uri()?>/images/other/review-product.jpg">
				</div>
				<div class="review--product-place">
					<div class="review--product-title">Укладка рулонного газона на готовое основание</div>
					<div class="review--product-price">от 49 руб./кв.м.*</div>
					<div class="show-more-link"><a href="">Заказать укладку</a></div>
				</div>
			</div>
			<div class="review-add-order">
				<div class="review-add-order-button-place--place">
					<div class="rating-count">4.5</div>
					<?include "inc/stars.php"?>
					<div class="rating-total-reviews">66 отзывов</div>
					<h3>Оставьте свой отзыв об этой услуге</h3>
				</div>
				<div class="review-add-order-button-place--button">
					<div class="big-green-button">Оставить отзыв</div>
				</div>
			</div>
			<div class="reviews">
				<div class="reviews--list">
					<?
					for ($i=0; $i < 10; $i++) { ?>
					<div class="reviews--item">
						<div class="reviews--avatar" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/avatar-demo.jpg)"></div>
						<div class="reviews--text-place">
							<div class="reviews--link">Отзыв о услуге: <a href="">Укладка рулонного газона на готовое основание</a></div>
							<div class="reviews-total">
								<div class="number-rating">4.5</div>
								<div class="stars"><img src="<?=get_stylesheet_directory_uri()?>/images/other/rating.png" alt=""></div>
							</div>
							<div class="reviews--people-info">
								<span>Игорь</span>
								<span>Организация</span>
							</div>
							<div class="reviews--review-text">Разнообразный и богатый опыт реализация намеченных плановых заданий способствует подготовки и реализации существенных финансовых и административных условий. Идейные соображения высшего порядка, а также постоянное информационно-пропагандистское обеспечение нашей деятельности позволяет оценить значение модели развития. Таким образом постоянный количественный рост и сфера нашей активности требуют определения и уточнения дальнейших направлений развития.</div>
						</div>
					</div>
					<?}?>
				</div>
				<div class="show-more-button">
					<a href="">Показать больше</a>
				</div>
			</div>

			<div class="space"></div>

			<h2 class="no-caps">Похожие услуги</h2>

			<div class="services-list-2">
				<?for ($i=0; $i < 2; $i++) { ?>
				<div class="services-list-2--item">
					<div class="services-list-2--img" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/demo-2.png)"></div>
					<div class="services-list-2--text">
						<h3>Укладка рулонного газона на готовое основание</h3>
						<p>Универсальный газон, подходит для широкого спектра озеленения</p>
						<div class="flex">
							<div class="module-stars"><img src="<?=get_stylesheet_directory_uri()?>/images/other/rating.png"></div>
							<div class="services-list-2--reviews">66 отзывов</div>						
						</div>
						<div class="services-list-2--price">от 149 руб./кв.м.</div>
						<div class="group">
							<div class="cell size-50">Цены указаны с НДС 20%</div>
							<div class="cell size-50"><div class="show-more-link"><a href="">Подробнее</a></div></div>
						</div>
					</div>
				</div>
				<?}?>
			</div>

			<div class="space"></div>

			<?include "inc/trust-block.php"?>
		</div>
	</div>

	<?include "inc/question-form.php"?>
	<?include "inc/article-determined-block.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>