<div class="realize-projects">
	<h2 class="h2-link">Реализованые проекты <a href="">все услуги</a></h2>
	<div class="slider--wr">
		<div class="realize-projects--slider">
			<?
			for ($i=0; $i < 6; $i++) { ?>
			<div class="item">
				<div class="image" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/project-demo.png)"></div>
				<ul class="blog-attributes">
					<li>Укладка</li>
					<li><a href="">85 м<sup>2</sup></a></li>
				</ul>
				<div class="title">Озеленение рулонным газоном крыши БЦ Москва</div>
			</div>
			<?}?>
		</div>
	</div>
</div>
<div class="show-mobile all-list-link"><a href="">Все проекты</a></div>