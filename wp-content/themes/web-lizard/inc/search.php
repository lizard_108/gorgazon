<div class="search">
	<form action="">
		<input type="text" placeholder="Поиск по сайту" value="" name="search" class="search--input">
		<div class="search--share js-open-share">
			<div class="share-open">
				<div class="share-open--wr">
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-facebook.svg"></a>
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-vk.svg"></a>
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-instagram.svg"></a>
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-od.svg"></a>
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-email.svg"></a>
				</div>
			</div>
		</div>
		<div class="search--calc js-open-calc"><a href="">Калькулятор</a></div>
	</form>
</div>