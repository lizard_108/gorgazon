<div class="question-form">
	<div class="question-form--text">
		<h2>Возникли вопросы?</h2>
		<p>Свяжитель с нами и мы <br>ответим на все ваши вопросы</p>
		<div class="question-form--phone">
			или позвоните по телефону <br>
			<b>+7 (495) 281-88-36</b>
		</div>
	</div>
	<div class="question-form--form-place">
		<form action="">
			<label class="dynamic-input">
				<div class="label">Телефон*</div>
				<input type="text" placeholder="Телефон*">
			</label>
			<label class="dynamic-input">
				<div class="label">Имя</div>
				<input type="text" placeholder="Имя">
			</label>
			<div class="group">
				<div class="cell size-50">
					<small>Нажимая на кнопку отправить,  вы даете согласие на обработку <a href="">персональных данных</a></small>
				</div>
				<div class="cell size-50">
					<div class="btn-green">Оставить заявку</div>
				</div>
			</div>
		</form>
	</div>
</div>