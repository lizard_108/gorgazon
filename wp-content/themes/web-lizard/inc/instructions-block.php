<div class="instructions-block">
	<h2 class="no-caps">Инструкции</h2>
	<div class="instructions-block--list">
		<div class="instructions-block--item">
			<div class="name">Самостоятельная укладка газона</div>
			<a href="" class="link">Читать</a>
		</div>
		<div class="instructions-block--item">
			<div class="name">Самостоятельный уход за газоном</div>
			<a href="" class="link">Читать</a>
		</div>
	</div>
</div>