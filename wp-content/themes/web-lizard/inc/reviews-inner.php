<h2 class="reviews-title">Отзывы <span>о укладке рулонного газона</span></h2>
<div class="reviews">
	<div class="reviews-total">
		<div class="number-rating">4.5</div>
		<div class="big-stars"><img src="<?=get_stylesheet_directory_uri()?>/images/other/big-stars.png" alt=""></div>
	</div>
	<div class="total-reviews-count">66 отзывов</div>
	<div class="reviews--list reviews-list-in-categoty">
		<?
		for ($i=0; $i < 3; $i++) { ?>
		<div class="reviews--item">
			<div class="reviews--avatar" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/avatar-demo.jpg)"></div>
			<div class="reviews--text-place">
				<div class="reviews--link">Отзыв о услуге: <a href="">Укладка рулонного газона на готовое основание</a></div>
				<div class="reviews-total">
					<div class="number-rating">4.5</div>
					<div class="stars">
						<div class="module-stars size-small" data-rating="4.5">
							<div class="module-stars--control"></div>
						</div>
					</div>
				</div>
				<div class="reviews--people-info">
					<span>Игорь</span>
					<span>Организация</span>
				</div>
				<div class="reviews--review-text">Разнообразный и богатый опыт реализация намеченных плановых заданий способствует подготовки и реализации существенных финансовых и административных условий. Идейные соображения высшего порядка, а также постоянное информационно-пропагандистское обеспечение нашей деятельности позволяет оценить значение модели развития. Таким образом постоянный количественный рост и сфера нашей активности требуют определения и уточнения дальнейших направлений развития.</div>
			</div>
		</div>
		<?}?>
	</div>
	<div class="show-more-button">
		<a href="">Показать больше</a>
	</div>
</div>