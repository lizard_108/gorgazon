<div class="modal-window modal-window--callback add-comment--fields-place">
	<div class="close"></div>
	<form id="callback-form">
		<div class="callback--subtitle">Обратный звонок</div>
		<div class="group">
			<div class="cell size-100">
				<label class="dynamic-input">
					<div class="label">Ваш телефон</div>
					<input type="text" placeholder="8 (999) 999 99 99">
				</label>
			</div>
		</div>
		<div class="group">
			<div class="cell size-100">
				<label class="dynamic-input">
					<div class="label">Имя</div>
					<input type="text" placeholder="Имя">
				</label>
			</div>
		</div>
		<div class="group callback--submit-place">
			<div class="cell size-100">
				<div class="btn-green">Оставить заявку</div>
			</div>
		</div>
		<div class="group">
			<div class="cell size-100">
				<small>Нажимая на кнопку отправить,  вы даете согласие на обработку <a href="">персональных данных</a></small>
			</div>
		</div>
	</form>
	<div class="modal-window--message-after-send">
		<p>Спасибо!</p>
		<p>Наши менеджеры свяжутся с Вами  в ближайшее время</p>
		<div class="btn-green js-close-modal-windows">Ок</div>
	</div>
</div>