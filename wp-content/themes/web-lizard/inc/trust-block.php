<div class="trust-block">
	<h2>Нам доверяют</h2>
	<div class="trust-block--slider">
		<div class="item">
			<div><img src="<?=get_stylesheet_directory_uri()?>/images/other/trust-1.png"></div>
			<div><img src="<?=get_stylesheet_directory_uri()?>/images/other/trust-3.png"></div>
		</div>
		<div class="item">
			<div><img src="<?=get_stylesheet_directory_uri()?>/images/other/trust-2.png"></div>
			<div><img src="<?=get_stylesheet_directory_uri()?>/images/other/trust-5.png"></div>
		</div>
		<div class="item">
			<div><img src="<?=get_stylesheet_directory_uri()?>/images/other/trust-4.png"></div>
			<div><img src="<?=get_stylesheet_directory_uri()?>/images/other/trust-7.png"></div>
		</div>
		<div class="item">
			<div><img src="<?=get_stylesheet_directory_uri()?>/images/other/trust-6.png"></div>
			<div><img src="<?=get_stylesheet_directory_uri()?>/images/other/trust-8.png"></div>
		</div>
		<div class="item">
			<div><img src="<?=get_stylesheet_directory_uri()?>/images/other/trust-6.png"></div>
			<div><img src="<?=get_stylesheet_directory_uri()?>/images/other/trust-8.png"></div>
		</div>
		<div class="item">
			<div><img src="<?=get_stylesheet_directory_uri()?>/images/other/trust-6.png"></div>
			<div><img src="<?=get_stylesheet_directory_uri()?>/images/other/trust-8.png"></div>
		</div>
	</div>
</div>