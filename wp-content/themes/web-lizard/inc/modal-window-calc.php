<div class="modal-window modal-window--calc">
	<div class="container">
		<div class="flex">
			<div class="calc--left-side">
				<div class="close-adapt-calc close"></div>
				<h2 class="no-caps">Калькулятор расчета стоимости газона</h2>
				<div class="modal--subtitle">
					Размеры участка
				</div>
				<div class="group">
					<div class="cell size-33">
						<span>Площадь участка, м2</span>
						<br>
						<small>не менее 50 кв, менее не работаем</small>
					</div>
					<div class="cell size-66">
						<label class="static-input">
							<input type="text" value="100" placeholder="">
						</label>
						<br>
						<div class="chechbox">
							<input type="checkbox" name="calc-1" id="calc-1">
							<label for="calc-1">
								+5% на обрезки
							</label>
						</div>
					</div>
				</div>
				<div class="modal--subtitle">
					Газон
				</div>
				<div class="group">
					<div class="cell size-33">
						<span>Вид газона</span>
					</div>
					<div class="cell size-66 two-radio">
						<div class="radio">
							<input type="radio" name="calc-2" id="calc-2-1">
							<label for="calc-2-1">
								Рулонный
							</label>
						</div>
						<div class="radio">
							<input type="radio" name="calc-2" id="calc-2-2">
							<label for="calc-2-2">
								Посевной
							</label>
						</div>
					</div>
				</div>
				<div class="group">
					<div class="cell size-33">
						<span>Сорт газона</span>
					</div>
					<div class="cell size-66">
						<select name="" id="">
							<option>Выбрать</option>
							<option value="">Сорт 1</option>
							<option value="">Сорт 2</option>
							<option value="">Сорт 4</option>
							<option value="">Сорт 5</option>
						</select>
					</div>
				</div>
				<div class="modal--subtitle">
					Услуги
				</div>
				<div class="group">
					<div class="cell size-33">
						<span>Укладка газона</span>
					</div>
					<div class="cell size-66">
						<div class="radio">
							<input type="radio" name="calc-3" id="calc-3-1">
							<label for="calc-3-1">
								Готовое основание
							</label>
						</div>
						<br>
						<div class="radio">
							<input type="radio" name="calc-3" id="calc-3-2">
							<label for="calc-3-2">
								Рекультивация
							</label>
						</div>
						<br>
						<div class="radio">
							<input type="radio" name="calc-3" id="calc-3-3">
							<label for="calc-3-3">
								Замена грунта
							</label>
						</div>
					</div>
				</div>
				<div class="group">
					<div class="cell size-33">
						<span>Бесплатный выезд замерщика</span>
					</div>
					<div class="cell size-66 two-radio">
						<div class="radio">
							<input type="radio" name="calc-4" id="calc-4-1">
							<label for="calc-4-1">
								Да
							</label>
						</div>
						<div class="radio">
							<input type="radio" name="calc-4" id="calc-4-2">
							<label for="calc-4-2">
								Нет
							</label>
						</div>
					</div>
				</div>
				<div class="modal--subtitle">
					Доставка
				</div>
				<div class="group">
					<div class="cell size-33">
						<span>Вид доставки</span>
					</div>
					<div class="cell size-66 two-radio">
						<div class="radio">
							<input type="radio" name="calc-5" id="calc-5-1">
							<label for="calc-5-1">
								Самовывоз
							</label>
						</div>
						<div class="radio">
							<input type="radio" name="calc-5" id="calc-5-2">
							<label for="calc-5-2">
								Доставка
							</label>
						</div>
					</div>
				</div>
				<div class="modal--subtitle">
					Контактная информация
				</div>
				<div class="group">
					<div class="cell size-33">
						<span>Телефон*</span>
					</div>
					<div class="cell size-66">
						<label class="static-input">
							<input type="text" value="" placeholder="+7 (987) 987 65 - 65">
						</label>
					</div>
				</div>
				<div class="group">
					<div class="cell size-33">
						<span>Имя</span>
					</div>
					<div class="cell size-66">
						<label class="static-input">
							<input type="text" value="" placeholder="Введите имя">
						</label>
					</div>					
				</div>
				<div class="space"></div>
				<div class="group">
					<div class="cell size-33">
						<div class="personal-info">
							Нажимая на кнопку оставить комментарий, вы даете согласие на обработку <b>персональных данных</b>
						</div>		
					</div>
					<div class="cell size-66">
						<div class="btn-green">Оставить заявку</div>
					</div>					
				</div>
			</div>
			<div class="calc--right-side">
				<div class="gray-place">
					<div class="calc--close"></div>
					<div class="calc--adapt-close js-close-adapt-calc-results"></div>
					<h3>Результат</h3>
					<table class="calc--result-list">
						<tr>
							<td>Площадь участка</td>
							<td>50 м2</td>
						</tr>
						<tr>
							<td>Запас, 5%</td>
							<td>-</td>
						</tr>
						<tr>
							<td>Сорт газона</td>
							<td>Спорт</td>
						</tr>
						<tr>
							<td>Стоимость газона:</td>
							<td>450 руб./рулон</td>
						</tr>
						<tr>
							<td>Количество рулонов</td>
							<td>5 шт</td>
						</tr>
						<tr>
							<td>Реультивация</td>
							<td>100 руб</td>
						</tr>
						<tr>
							<td>Укладка</td>
							<td>150 руб</td>
						</tr>
						<tr>
							<td>Накладные расходы</td>
							<td>2 250 руб</td>
						</tr>
						<tr>
							<td>Доставка</td>
							<td>-</td>
						</tr>
					</table>
				</div>
				<div class="green-place">
					<div class="calc-question js-open-adapt-calc-results"></div>
					<table>
						<tr>
							<td>Итого:</td>
							<td class="final">50 450 руб</td>
						</tr>
					</table>
				</div>
				<div class="green2-place">
					<table>
						<tr>
							<td>Скачать</td>
							<td><a href="">PDF</a></td>
						</tr>
						<tr>
							<td>Отправить результат</td>
							<td><a href="">Mail</a></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>