<h3 class="no-caps">2 комментария</h3>

<div class="add-comment js-open-comment-form">
	<input type="text" placeholder="Написать комментарий">
	<a href="#" class="comments--attach"></a>
</div>

<div class="add-comment--open-form">
	<div class="add-comment--textarea">
		<textarea name="">Разнообразный и богатый опыт реализация намеченных плановых заданий способствует подготовки</textarea>
		<a href="#" class="comments--attach"></a>
	</div>
	<div class="add-comment--fields-place">
		<div class="group">
			<div class="cell size-33"><span>Ваше имя</span></div>
			<div class="cell size-66">
				<label class="static-input">
					<input type="text" value="" placeholder="Например: Андрей">
				</label>
			</div>
		</div>
		<div class="group">
			<div class="cell size-33"><span>Электронная почта</span></div>
			<div class="cell size-66">
				<label class="static-input">
					<input type="text" value="" placeholder="Введите вашу почту">
				</label>
			</div>
		</div>
		<div class="group">
			<div class="cell size-33"><span>или войдите через соц. сеть</span></div>
			<div class="cell size-66">
				<div class="social">
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-facebook.svg"></a>
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-vk.svg"></a>
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-instagram.svg"></a>
				</div>
				<div class="captcha">
					<img src="<?=get_stylesheet_directory_uri()?>/images/other/captcha.png">
				</div>
				<div class="group">
					<div class="cell size-33">
						<div class="btn-gray">Отмена</div>
					</div>
					<div class="cell size-66">
						<div class="btn-green">Оставить комментарий</div>
					</div>
				</div>
				<div class="personal-info">
					Нажимая на кнопку оставить комментарий, вы даете согласие на обработку <b>персональных данных</b>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="comments-block">
	<div class="comments-list">
		<div class="comments-list--item">
			<div class="comments--avatar" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/avatar-demo.jpg)"></div>
			<div class="comments--text-place">
				<div class="comments--name">Игорь</div>
				<div class="comments--date">31 августа 2019</div>
				<div class="comments--text">Разнообразный и богатый опыт реализация намеченных плановых заданий способствует подготовки и реализации существення.</div>
				<div class="comments-attach">
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/other/attach-1.jpg"></a>
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/other/attach-2.jpg"></a>
				</div>
			</div>
		</div>
		<div class="comments-list--item">
			<div class="comments--avatar" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/avatar-demo.jpg)"></div>
			<div class="comments--text-place">
				<div class="comments--name">Игорь</div>
				<div class="comments--date">31 августа 2019</div>
				<div class="comments--text">Разнообразный и богатый опыт реализация намеченных плановых заданий способствует подготовки и реализации существення.</div>
				<div class="comments-attach">
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/other/attach-1.jpg"></a>
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/other/attach-2.jpg"></a>
				</div>
			</div>
		</div>
	</div>
</div>