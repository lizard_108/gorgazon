<?php
/**
 * Template Name: страница "продукция - 2"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	

		<div class="services-submenu">
			<div class="services-submenu--title">Продукция</div>
			<ul class="services-submenu--menu">
				<li class="current"><a href="">Рулонный газон</a></li>
				<li><a href="">Семена газонных трав</a></li>
				<li><a href="">Посадочный материал</a></li>
				<li><a href="">Тротуарная плитка</a></li>
				<li><a href="">Газонный грунт</a></li>
				<li><a href="">Удобрения для газона</a></li>
			</ul>
		</div>
		
		<div class="main-content">
			<h1>Рулонный газон</h1>

			<div class="page-text">
				Мы постарались подготовить для своих клиентов <b>доступные цены</b> на рулонный газон, состоящий из сортов первоклассного мятлика американской селекции.
			</div>

			<?include "inc/download-price-block.php"?>

			<br>
			<br>
			<br>
			<?for ($d=0; $d < 4; $d++) { ?>
			<h2 class="no-caps">Декоративные рулонные газоны</h2>
			<div class="services-list-2">
				<?for ($i=0; $i < 2; $i++) { ?>
				<div class="services-list-2--item">
					<div class="services-list-2--img" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/demo-2.png)"></div>
					<div class="services-list-2--text">
						<h3>Тень Стандарт</h3>
						<p>Газон для территорий с дефицитом освещения</p>
						<div class="flex">
							<?include "inc/stars.php"?>
							<div class="services-list-2--reviews">66 отзывов</div>						
						</div>
						<div class="services-list-2--price">104 руб./рулон</div>
						<div class="services-list-2--price-2">116 руб./м2</div>
						<div class="group">
							<div class="cell size-50">Цены указаны с НДС 20%</div>
							<div class="cell size-50"><div class="show-more-link"><a href="">Подробнее</a></div></div>
						</div>
					</div>
				</div>
				<?}?>
			</div>
			<?}?>

			<div class="gaz-banner">
				<h3>Рулонный газон оптом</h3>
				<p>Мы готовы предложить одни из лучших цен на рулонный газон <br>на российском рынке, высокое качество газона и поставку точно в срок.</p>
				<div class="show-more-link"><a href="">Подробнее</a></div>
			</div>

			<?include "inc/trust-block.php"?>
			<?include "inc/white-info-block.php"?>

			<div class="space"></div>
			<h2 class="no-caps">Параметры упаковки рулонного газона</h2>
			<div class="group adapt">
				<div class="cell size-50">
					<ul class="line-list">
						<li>Газон уложен на европаллет (1х1,2 м) и обмотан стрейч пленкой.</li>
						<li>Один паллет равен 50 кв.м. или 63 рулонам газона.</li>
						<li>Один рулон весит в среднем около 20 кг (зависит от количества осадков).</li>
					</ul>
				</div>
				<div class="cell size-50">
					<p>Стоимость доставки не включена в стоимость газона (за исключением специальных предложений и акций) и рассчитывается отдельно. За время работы нам удалось найти решение по существенной экономии стоимости доставки.</p>
					<p>Мы принципиально не зарабатываем на доставке нашего газона!</p>
					<p>При необходимости мы готовы организовать дешевую экспресс доставку рулонного газона в любую точку европейскую части России.</p>
				</div>
			</div>
			<div class="space"></div>

			<!-- /// -->
			<h2 class="reviews-title">Отзывы <span>о рулонном газоне</span></h2>
			<div class="reviews">
				<div class="reviews-total">
					<div class="number-rating">4.5</div>
					<div class="big-stars"><img src="<?=get_stylesheet_directory_uri()?>/images/other/big-stars.png" alt=""></div>
				</div>
				<div class="total-reviews-count">66 отзывов</div>
				<div class="reviews--list reviews-list-in-categoty">
					<?
					for ($i=0; $i < 3; $i++) { ?>
					<div class="reviews--item">
						<div class="reviews--avatar" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/avatar-demo.jpg)"></div>
						<div class="reviews--text-place">
							<div class="reviews--link">Отзыв о услуге: <a href="">Укладка рулонного газона на готовое основание</a></div>
							<div class="reviews-total">
								<div class="number-rating">4.5</div>
								<div class="stars"><img src="<?=get_stylesheet_directory_uri()?>/images/other/rating.png" alt=""></div>
							</div>
							<div class="reviews--people-info">
								<span>Игорь</span>
								<span>Организация</span>
							</div>
							<div class="reviews--review-text">Разнообразный и богатый опыт реализация намеченных плановых заданий способствует подготовки и реализации существенных финансовых и административных условий. Идейные соображения высшего порядка, а также постоянное информационно-пропагандистское обеспечение нашей деятельности позволяет оценить значение модели развития. Таким образом постоянный количественный рост и сфера нашей активности требуют определения и уточнения дальнейших направлений развития.</div>
						</div>
					</div>
					<?}?>
				</div>
				<div class="show-more-button">
					<a href="">Показать больше</a>
				</div>
			</div>
			<!-- /// -->
			<div class="calc-banner">
				<h3>Калькулятор стоимости</h3>
				<p>Проведите предварительный расчет стоимости</p>
				<div class="show-more-link"><a href="">Рассчитать</a></div>
			</div>

			<div class="delevery-banner">
				<h3>Доставка</h3>
				<p>Обратите внимание на условия доставки рулонного газона</p>
				<div class="show-more-link"><a href="">Подробнее</a></div>
			</div>

			<?include "inc/garanty-and-pay-blocks.php"?>
			
		</div>
	</div>

	<?include "inc/question-form.php"?>
	<?include "inc/article-determined-block.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>