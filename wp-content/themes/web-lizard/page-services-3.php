<?php
/**
 * Template Name: страница "Услуги - 3 - Укладка рулонного газона - Укладка рулонного газона  на готовое основание"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	

		<div class="services-submenu">
			<div class="services-submenu--title">Укладка рулонного газона</div>
			<ul class="services-submenu--menu">
				<li class="current"><a href="">Укладка рулонного газона на готовое основание</a></li>
				<li><a href="">Укладка рулонного газона под ключ на готовое основание</a></li>
				<li class="last"><a href="">Ещё</a></li>
			</ul>
		</div>
		
		<div class="main-content">
			<h1>Укладка рулонного газона на готовое основание </h1>
			<div class="services-product">
				<div class="services-product--image" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/demo-3.png)"></div>
				<div class="services-product--place">
					<div class="flex services-product-rating">
						<div><strong>4.5</strong></div>
						<?include "inc/stars.php"?>
						<div class="services-list-2--reviews">66 отзывов</div>						
					</div>
					<div class="services-product--term">
						Срок выполнения - <b>от 1 дня<sup>**</sup></b>
					</div>
					<div class="services-product--price">
						от 49 руб./кв.м.*
					</div>
					<div class="services-product--subprice">Цены указаны с НДС 20%</div>
					<div class="services-product--buttons group">
						<div class="cell size-50">
							<div class="btn-green">Заказать укладку</div>
						</div>
						<div class="cell size-50">
							<div class="btn-transparent">Вызов озеленителя</div>
						</div>
					</div>
				</div>
			</div>
			<div class="services-product--small-info">
				<p>*Приведена ориентировочная цена для участка площадью 10000 кв.м. Стоимость указана без учета стоимости материалов.</p>
				<p>**Сроки выполнения работ оговариваются индивидуально. При необходимости все работы могут быть выполнены срочно.</p>
			</div>


			<ul class="tab-switcher">
				<li data-open="1" class="on">Описание</li>
				<li data-open="2">Преимущества</li>
				<li data-open="3">Стоимость газона</li>
				<li data-open="4">Отзывы</li>
				<li data-open="5">Выезд озеленителя</li>
			</ul>


			<div class="hidden-sections">
				<div class="section">
					<h2>Описание работ</h2>
					<div class="group">
						<div class="cell size-50">
							<ul class="line-list">
								<li>Рыхление верхнего слоя основания (5-10мм);</li>
								<li>Укладка рулонного газона;</li>
								<li>Послеукладочные процедуры.</li>
							</ul>
						</div>
						<div class="cell size-50">
							<p>Стоимость укладки рулонного газона формируется исходя из множества факторов, которые необходимо учесть при составлении сметы.</p>
						</div>
					</div>
				</div>
				<div class="section">
					<h2>Преимущества</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt commodi iusto temporibus voluptas modi quis iste minima doloremque, totam, excepturi nobis! Aliquam reiciendis vitae id debitis in officia, consequatur velit.</p>
				</div>
			</div>

			<div class="space"></div>	
			<?include "inc/green-block.php"?>
			<div class="space"></div>	
			<?include "inc/benefits-block.php"?>
			<div class="space"></div>	
			<?include "inc/price-table.php"?>	
			<?include "inc/reviews-inner.php"?>
			<div class="space"></div>
			<?include "inc/calc-banner.php"?>
			<?include "inc/spec-green-block.php"?>
			<div class="space"></div>
			<?include "inc/instructions-block.php"?>
			<div class="space"></div>
			<?include 'inc/realize-projects.php';?>

			<?include "inc/garanty-and-pay-blocks.php"?>

			<?include "inc/trust-block.php"?>	
		</div>
	</div>

	<?include "inc/question-form.php"?>
	<?include "inc/article-determined-block.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>