<?php
/**
 * Template Name: страница "Доставка 2"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	
		<div class="main-content">
			<h1 class="no-caps">Московская область</h1>
			<div class="maintext">
				<p>Рулонный газон достаточно тяжеловесный груз. <strong>1 поддон (паллет)</strong> рулонного газона <strong>вмещает в себя около 50 кв.м. газона или 63 рулона</strong>, вес одного поддона газона около 1,5 т. Поэтому <strong>стоимость доставки очень существенна в заказе газона</strong>.</p>
			</div>
			<br>

			<div class="page-search">
				<input type="text" placeholder="Введите название округа">
				<div class="submit"></div>
			</div>

			<h2 class="no-caps">Районы Московской области</h2>
			<div class="delivery-list">
				<div class="group">
					<div class="cell size-50">
						<ul class="article-list">
							<li><a href="">Волоколамский район</a></li>
							<li><a href="">Дмитровский район</a></li>
							<li><a href="">Зарайский район</a></li>
							<li><a href="">Каширский район</a></li>
							<li><a href="">Коломенский район</a></li>
							<li><a href="">Ленинский район</a></li>
							<li><a href="">Луховицкий район</a></li>
							<li><a href="">Можайский район</a></li>
							<li><a href="">Наро-Фоминский район</a></li>
							<li><a href="">Одинцовский район</a></li>
							<li><a href="">Орехово-Зуевский район</a></li>
							<li><a href="">Подольский район</a></li>
							<li><a href="">Раменский район</a></li>
							<li><a href="">Сергиево-Посадский район</a></li>
							<li><a href="">Серпуховский район</a></li>
							<li><a href="">Ступинский район</a></li>
							<li><a href="">Чеховский район</a></li>
							<li><a href="">Шаховской район</a></li>
						</ul>
					</div>
					<div class="cell size-50">
						<ul class="article-list">
							<li><a href="">Воскресенский район</a></li>
							<li><a href="">Егорьевский район</a></li>
							<li><a href="">Истринский район</a></li>
							<li><a href="">Клинский район</a></li>
							<li><a href="">Красногорский район</a></li>
							<li><a href="">Лотошинский район</a></li>
							<li><a href="">Люберецкий район</a></li>
							<li><a href="">Мытищинский район</a></li>
							<li><a href="">Ногинский район</a></li>
							<li><a href="">Озёрский район</a></li>
							<li><a href="">Павлово-Посадский район</a></li>
							<li><a href="">Пушкинский район</a></li>
							<li><a href="">Рузский район</a></li>
							<li><a href="">Серебряно-Прудский район</a></li>
							<li><a href="">Солнечногорский район</a></li>
							<li><a href="">Талдомский район</a></li>
							<li><a href="">Шатурский район</a></li>
							<li><a href="">Щёлковский район</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="space"></div>
			<h2 class="no-caps">Приволжский федеральный округ</h2>
			<div class="delivery-list">
				<div class="group">
					<div class="cell size-50">
						<ul class="article-list">
							<li><a href="">город Москва</a></li>
							<li><a href="">Московская область</a></li>
							<li><a href="">Белгородская область</a></li>
							<li><a href="">Брянская область</a></li>
							<li><a href="">Владимирская область</a></li>
							<li><a href="">Воронежская область</a></li>
							<li><a href="">Ивановская область</a></li>
							<li><a href="">Калужская область</a></li>
							<li><a href="">Костромская область</a></li>
						</ul>
					</div>
					<div class="cell size-50">
						<ul class="article-list">
							<li><a href="">Курская область</a></li>
							<li><a href="">Липецкая область</a></li>
							<li><a href="">Орловская область</a></li>
							<li><a href="">Рязанская область</a></li>
							<li><a href="">Смоленская область</a></li>
							<li><a href="">Тамбовская область</a></li>
							<li><a href="">Тверская область</a></li>
							<li><a href="">Тульская область</a></li>
							<li><a href="">Ярославская область</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="space"></div>
			<h2 class="no-caps">Южный федеральный округ</h2>
			<div class="delivery-list">
				<div class="group">
					<div class="cell size-50">
						<ul class="article-list">
							<li><a href="">город Москва</a></li>
							<li><a href="">Московская область</a></li>
							<li><a href="">Белгородская область</a></li>
							<li><a href="">Брянская область</a></li>
							<li><a href="">Владимирская область</a></li>
							<li><a href="">Воронежская область</a></li>
							<li><a href="">Ивановская область</a></li>
							<li><a href="">Калужская область</a></li>
							<li><a href="">Костромская область</a></li>
						</ul>
					</div>
					<div class="cell size-50">
						<ul class="article-list">
							<li><a href="">Курская область</a></li>
							<li><a href="">Липецкая область</a></li>
							<li><a href="">Орловская область</a></li>
							<li><a href="">Рязанская область</a></li>
							<li><a href="">Смоленская область</a></li>
							<li><a href="">Тамбовская область</a></li>
							<li><a href="">Тверская область</a></li>
							<li><a href="">Тульская область</a></li>
							<li><a href="">Ярославская область</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="space"></div>
			<h2 class="no-caps">Северо-Западный федеральный округ</h2>
			<div class="delivery-list">
				<div class="group">
					<div class="cell size-50">
						<ul class="article-list">
							<li><a href="">город Москва</a></li>
							<li><a href="">Московская область</a></li>
							<li><a href="">Белгородская область</a></li>
							<li><a href="">Брянская область</a></li>
							<li><a href="">Владимирская область</a></li>
							<li><a href="">Воронежская область</a></li>
							<li><a href="">Ивановская область</a></li>
							<li><a href="">Калужская область</a></li>
							<li><a href="">Костромская область</a></li>
						</ul>
					</div>
					<div class="cell size-50">
						<ul class="article-list">
							<li><a href="">Курская область</a></li>
							<li><a href="">Липецкая область</a></li>
							<li><a href="">Орловская область</a></li>
							<li><a href="">Рязанская область</a></li>
							<li><a href="">Смоленская область</a></li>
							<li><a href="">Тамбовская область</a></li>
							<li><a href="">Тверская область</a></li>
							<li><a href="">Тульская область</a></li>
							<li><a href="">Ярославская область</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="space"></div>
			<h2 class="no-caps">Северо-Кавказский федеральный округ</h2>
			<div class="delivery-list">
				<div class="group">
					<div class="cell size-50">
						<ul class="article-list">
							<li><a href="">город Москва</a></li>
							<li><a href="">Московская область</a></li>
							<li><a href="">Белгородская область</a></li>
							<li><a href="">Брянская область</a></li>
							<li><a href="">Владимирская область</a></li>
							<li><a href="">Воронежская область</a></li>
							<li><a href="">Ивановская область</a></li>
							<li><a href="">Калужская область</a></li>
							<li><a href="">Костромская область</a></li>
						</ul>
					</div>
					<div class="cell size-50">
						<ul class="article-list">
							<li><a href="">Курская область</a></li>
							<li><a href="">Липецкая область</a></li>
							<li><a href="">Орловская область</a></li>
							<li><a href="">Рязанская область</a></li>
							<li><a href="">Смоленская область</a></li>
							<li><a href="">Тамбовская область</a></li>
							<li><a href="">Тверская область</a></li>
							<li><a href="">Тульская область</a></li>
							<li><a href="">Ярославская область</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="space"></div>
			<div class="avantgardes-block">
				<div class="group">
					<div class="cell size-50">
						<h3 class="no-caps">Преимущества нашей доставки рулонного газона</h3>
					</div>
					<div class="cell size-50">
						<h4 class="text-green">Низкая стоимость доставки</h4>
						<p>Газона в любую точку европейской части России. Это означает, что интересы фирмы не предусматривают завышение транспортных расходов, более того, часть затрат мы берем на себя, а вы в результате сможете получить рулонный газон исключительного качества по адекватной цене.</p>
						<h4 class="text-green">Дополнительный сервис</h4>		
						<p>Вместе с оперативной доставкой, компания «Горгазон» также предлагает дополнительный сервис: укладку рулонного газона. Доверившись специалистам, вы не только получите идеальное ландшафтное покрытие, но и избежите распространенных трудностей и ошибок при самостоятельном выращивании газона.</p>				
						<h4 class="text-green">Экспресс-доставка</h4>		
						<p>рулонного газона поможет реально выиграть время: возможность как можно быстрее высадить травяной слой существенно уменьшит срок адаптации газона. Это также повлияет на качество уже готового газонного покрытия, трава попросту не успеет завянуть, корневая система будет максимально жизнеспособной. Особенно это актуально в летнюю жару, когда продолжительность жизни срезанного газона не может превышать 4 суток.</p>
					</div>
				</div>
			</div>
			<div class="space"></div>
			<h3 class="no-caps">Обратившись в компанию «Горгазон», вы выигрываете дважды:</h3>
			<ul class="line-list">
				<li>первоклассный рулонный газон по честной цене;</li>
				<li>оперативная и дешевая доставка рулонного газона распространяется на любую точку европейской части России — мы принципиально не зарабатываем на доставке!</li>
			</ul>
		</div>
	</div>
	
	<?include "inc/question-form.php"?>
	<?include "inc/article-determined-block.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>