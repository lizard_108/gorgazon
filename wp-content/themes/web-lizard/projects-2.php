<?php
/**
 * Template Name: страница "Проекты 2"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	
		<div class="main-content">
			<h1 class="no-caps">Детская площадка</h1>
			<ul class="blog-attributes on-blog-page">
				<li>Укладка</li>
				<li><a href="">85 м2</a></li>
			</ul>
			<div class="space"></div>

			<div class="blog-page-inner">
				<div class="blog-page-inner--image">
					<img src="<?=get_stylesheet_directory_uri()?>/images/other/blog-1.jpg">
				</div>
				<div class="space"></div>
				<div class="blog-page-inner--text">
					<h3>Как формируется стоимость газона под ключ</h3>
					<p>Стоимость газона под ключ рассчитывается исходя из стоимости маткриала (рулонов газона) и работ, необходимых для завершения формирования непосредственно самого газона.</p>
					<p>Если со стоимостью рулонного газона все предельно ясно: цену на них всегда можно посмотреть в прайс-листах, то с работой все не так однозначно. Даже для сходных по площади участков цена на их озеленение может существенно разниться.</p>
					<p>Происходить это может по множеству причин. Например, сложная геометрия участка. Вполне очевидно, что прямые линии, отсутствие неровностей рельефа территории, предназначенной для озеленения, существенно уменьшают трудозатраты на ее озеленение и, соответственно, ее стоимость.</p>
					<p>Стоимость работ по устройству газона на участках с высотными перепадами, со множеством каких-либо элементов (клумб, дорожек, построек и т.д.), сложной геометрией закономерно возрастает.</p>
				</div>
				<div class="space"></div>
				<div class="blog-page-inner--text">
					<p>Стоимость газона под ключ рассчитывается исходя из стоимости маткриала (рулонов газона) и работ, необходимых для завершения формирования непосредственно самого газона.</p>
				</div>
				<div class="space"></div>
				<div class="blog-page-inner--image">
					<img src="<?=get_stylesheet_directory_uri()?>/images/other/blog-3.jpg">
				</div>
				<br>
				<div class="blog-page-inner--image">
					<img src="<?=get_stylesheet_directory_uri()?>/images/other/blog-4.jpg">
				</div>
				<div class="space"></div>
				<div class="blog-page-inner--text">
					<p>Стоимость газона под ключ рассчитывается исходя из стоимости маткриала (рулонов газона) и работ, необходимых для завершения формирования непосредственно самого газона.</p>
				</div>
			</div>
			<div class="space"></div>
			<div class="space"></div>

			<?
			include 'inc/realize-projects.php';
			?>
		</div>
	</div>
	
	<?include "inc/question-form.php"?>
	<?include "inc/article-determined-block.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>