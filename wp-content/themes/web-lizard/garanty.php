<?php
/**
 * Template Name: страница "Гарантия"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	
		<div class="main-content">
			<h1>Гарантия</h1>
			<h3>Как формируется стоимость газона под ключ</h3>
			<p>Стоимость газона под ключ рассчитывается исходя из стоимости маткриала (рулонов газона) и работ, необходимых для завершения формирования непосредственно самого газона.</p>
			<p>Если со стоимостью рулонного газона все предельно ясно: цену на них всегда можно посмотреть в прайс-листах, то с работой все не так однозначно. Даже для сходных по площади участков цена на их озеленение может существенно разниться.</p>
			<p>Происходить это может по множеству причин. Например, сложная геометрия участка. Вполне очевидно, что прямые линии, отсутствие неровностей рельефа территории, предназначенной для озеленения, существенно уменьшают трудозатраты на ее озеленение и, соответственно, ее стоимость.</p>
			<p>Стоимость работ по устройству газона на участках с высотными перепадами, со множеством каких-либо элементов (клумб, дорожек, построек и т.д.), сложной геометрией закономерно возрастает.</p>
			<br>
			<div class="garanty-blocks">
				<div class="group align-center order-1">
					<div class="cell size-50">
						<h3>Кем и как определяется стоимость устройства газона</h3>
						<p>В отличие от остальных компаний, которые для упрощения своей деятельности заведомо завышают цены на свои услуги до максимума, компания “Горгазон” исповедует совсем другой подход.</p>
						<p>Стоимость укладки рулонного газона в Москве и области (до 50 км от МКАД) определяет профессиональный озеленитель, выезд которого бесплатен. Специалист по благоустройству и озеленению произведет все необходимые замеры, с целью минимизировать затраты клиента на укладку рулонного газона.</p>
					</div>
					<div class="cell size-50">
						<img src="<?=get_stylesheet_directory_uri()?>/images/other/demo-5.jpg">
					</div>
				</div>
				<div class="space"></div>
				<div class="group align-center order-2">
					<div class="cell size-50">
						<img src="<?=get_stylesheet_directory_uri()?>/images/other/demo-5.jpg">
					</div>
					<div class="cell size-50">
						<p>Итогом работы озеленителя является список рекомендованных услуг и цен на них – смета, которые, после согласования с клиентом и станут основным ориентиром, при формирование полной стоимости работ по устройству газона. Естественно, любые из процедур, указанных в полученной смете можно как корректировать, так и выполнить собственными силами.</p>
						<p>Стоимость устройства газона</p>
						<p>Выезд озеленителя на участок во многих случаях позволяет существенно снизить стоимость конечной работы и делает наш подход выгодным для клиента. Все, что необходимо для того, чтобы воспользоваться данной услугой – положить на свой депозит в компании “Горгазон” 3000 рублей. Впоследствии, вы можете потратить ее по своему усмотрению: как на покупку рулонов газона, так и на оплату услуг по укладке газона.</p>
					</div>
				</div>
				<div class="space"></div>
			</div>

			<h3>Стоимость укладки газона на готовое основание</h3>

			<p>Если основание вашего участка уже должным образом подготовлено, то стоимость устройства рулонного газона будет ниже, чем с обустройством основания, но, к сожалению, поскольку специалисты компании “Горгазон” не принимали участия в его подготовке и контроле, то и гарантий на него дать никаких мы не сможем.</p>
			<p>По этой причине, если вы еще не определились, выполнить часть работ самостоятельно или обратиться за данной услугой к нашим специалистам, то мы настоятельно рекомендуем вам выбрать второй вариант: именно в этом случае, несмотря на то, что стоимость укладки газона за метр незначительно возрастет, вы получите полный пакет гарантий на ваш новый газон.</p>
			<p>Тем не менее, если вы решили подготовить основание самостоятельно или обратились к другим компаниям до того, как узнали о нас, не беда: “Горгазон” производит укладку газона на уже готовое основание.</p>
			<div class="space"></div>
		</div>
	</div>
	
	<?include "inc/question-form.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>