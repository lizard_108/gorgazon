<?php
/**
 * Template Name: страница "Продукция - 3"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	

		<div class="services-submenu">
			<div class="services-submenu--title">Декоративные рулонные газоны</div>
			<ul class="services-submenu--menu align-left">
				<li class="current"><a href="">Стандарт</a></li>
				<li><a href="">Первый сорт</a></li>
				<li><a href="">Люкс</a></li>
			</ul>
		</div>
		
		<div class="main-content">
			<h1>Стандартный рулонный газон</h1>
			<div class="services-product product-edition">
				<div class="services-product--slider">
					<div class="js-services-product--slider">
						<div class="item" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/product-demo.jpg)"></div>
						<div class="item" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/product-demo.jpg)"></div>
						<div class="item" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/product-demo.jpg)"></div>
						<div class="item" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/product-demo.jpg)"></div>
					</div>
					<div class="services-product--slider--subslider">
						<div class="item current" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/product-demo.jpg)"></div>
						<div class="item" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/product-demo.jpg)"></div>
						<div class="item" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/product-demo.jpg)"></div>
						<div class="item" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/product-demo.jpg)"></div>
					</div>
				</div>
				<div class="services-product--place">
					<div class="flex services-product-rating">
						<div><strong>4.5</strong></div>
						<?include "inc/stars.php"?>
						<div class="services-list-2--reviews">66 отзывов</div>						
					</div>
					<div class="services-product--description">Это универсальный сорт газона. Отлично подойдет для благоустройства городских лужаек и скверов.</div>
					<div class="services-product--price">
						116 руб./рулон (0,8 м2)
					</div>
					<div class="services-product--price-2">145 руб./м2</div>
					<div class="services-product--subprice">Цены указаны с НДС 20%</div>
					<div class="services-product--buttons group">
						<div class="cell size-50">
							<div class="btn-green">Заказать газон</div>
						</div>
						<div class="cell size-50">
							<div class="btn-transparent">Заказать газон с укладкой</div>
						</div>
					</div>
				</div>
			</div>
			<div class="space"></div>

			<ul class="tab-switcher">
				<li data-open="1" class="on">Описание</li>
				<li data-open="2">доставка</li>
				<li data-open="3">Отзывы</li>
				<li data-open="4">Инструкции</li>
				<li data-open="5">Оплата</li>
				<li data-open="6">Гарантия</li>
			</ul>


			<div class="hidden-sections">
				<div class="section">
					<h2 class="no-caps">Описание</h2>
					<ul class="line-list">
						<li>Газон зеленого цвета с небольшими светло-зелеными вкраплениями*.</li>
						<li>Допускается наличие примесей прочих злаковых не более 3%.</li>
						<li>Без каких-либо дефектов травяного покрова*.</li>
						<li>Адаптирован к российским климатическим условиям.</li>
					</ul>
					<p><small>*Данное условие применимо в летний периода, при среднесуточной температуре не менее 20 градусов по Цельсию и наличию осадков на протяжении не менее 14 суток.</small></p>
					<div class="space"></div>
					<h2 class="no-caps">Параметры упаковки рулонного газона</h2>
					<ul class="line-list">
						<li>Газон уложен на европаллет (1х1,2 м) и обмотан стрейч пленкой.</li>
						<li>Один паллет равен 50 кв.м. или 63 рулонам газона.</li>
						<li>Один рулон весит в среднем около 20 кг (зависит от количества осадков).</li>
					</ul>
					<div class="space"></div>
					<h2 class="no-caps">Технические характеристики</h2>
					<ul class="line-list">
						<li>Возраст: 1-2 года</li>
						<li>Рулон – 0,4х2 м. (0,8 кв.м.)</li>
						<li>Толщина дернины – 2 см</li>
					</ul>
					<div class="space"></div>
					<h2 class="no-caps">Состав посевной смеси</h2>
					<ul class="line-list">
						<li>100% Селекционный мятлик (Poa pratensis)</li>
					</ul>
					<div class="space"></div>
					<h2 class="no-caps">Качество газона</h2>
					<ul class="line-list">
						<li>специально обрабатываемые поля, на которых выращивается трава высокого качества;</li>
						<li>спецтехника, предназначенная для посева, ухода и среза;</li>
						<li>продуманная система орошения;</li>
						<li>и многое другое.</li>
					</ul>
					<div class="space"></div>
				</div>
				<div class="section">
					<h2>Преимущества</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt commodi iusto temporibus voluptas modi quis iste minima doloremque, totam, excepturi nobis! Aliquam reiciendis vitae id debitis in officia, consequatur velit.</p>
				</div>
			</div>

			<div class="space"></div>	
			<?include "inc/green-block.php"?>
			<div class="space"></div>	
			<?include "inc/white-info-block.php"?>
			<div class="space"></div>	


			<div class="delevery-banner">
				<h3>Доставка</h3>
				<p>Обратите внимание на условия доставки рулонного газона</p>
				<div class="show-more-link"><a href="">Подробнее</a></div>
			</div>


			<?include "inc/price-table.php"?>	


			<?include "inc/reviews-inner.php"?>
			<?include "inc/instructions-block.php"?>
			<div class="space"></div>

			<?include "inc/calc-banner.php"?>
			<?include "inc/garanty-and-pay-blocks.php"?>
			<div class="space"></div>

			<h2 class="no-caps">Похожие товары</h2>
			<div class="services-list-2">
				<?for ($i=0; $i < 3; $i++) { ?>
				<div class="services-list-2--item">
					<div class="services-list-2--img" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/demo-2.png)"></div>
					<div class="services-list-2--text">
						<h3>Тень Стандарт</h3>
						<p>Газон для территорий с дефицитом освещения</p>
						<div class="flex">
							<?include "inc/stars.php"?>
							<div class="services-list-2--reviews">66 отзывов</div>						
						</div>
						<div class="services-list-2--price">104 руб./рулон</div>
						<div class="services-list-2--price-2">116 руб./м2</div>
						<div class="group">
							<div class="cell size-50">Цены указаны с НДС 20%</div>
							<div class="cell size-50"><div class="show-more-link"><a href="">Подробнее</a></div></div>
						</div>
					</div>
				</div>
				<?}?>
			</div>
			<?include "inc/trust-block.php"?>
		</div>
	</div>

	<?include "inc/question-form.php"?>
	<?include "inc/article-determined-block.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>