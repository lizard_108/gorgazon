<?php
/**
 * Template Name: страница "Услуги"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	
		
		<div class="main-content">
			<h1>Услуги</h1>
			<p>Мы предлагаем широкий спектр услуг по <b>созданию</b> газонов, <b>озеленению</b>  и последующему <b>обслуживанию</b> объектов озеленения.</p>
			<div class="services-list">
				<div class="services-list--item">
					<div class="services-list--img" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/demo-1.png)"><a href=""></a></div>
					<div class="service-list--text">
						<h3>Укладка рулонного газона</h3>
						<p>Профессиональная укладка рулонного газона. 100% гарантия приживаемости. Срок выполнения работ от 1 дня.</p>
						<div class="show-more-link"><a href="">Подробнее</a></div>
					</div>
					<div class="service-list--price">
						от 49 руб./кв.м. <br><small>Цены указаны с НДС 20%</small>
					</div>
				</div>
				<div class="services-list--item">
					<div class="services-list--img" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/demo-1.png)"><a href=""></a></div>
					<div class="service-list--text">
						<h3>Укладка рулонного газона</h3>
						<p>Профессиональная укладка рулонного газона. 100% гарантия приживаемости. Срок выполнения работ от 1 дня.</p>
						<div class="show-more-link"><a href="">Подробнее</a></div>
					</div>
					<div class="service-list--price">
						от 499 999 руб./кв.м. <br><small>Цены указаны с НДС 20%</small>
					</div>
				</div>
				<div class="services-list--item">
					<div class="services-list--img" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/demo-1.png)"><a href=""></a></div>
					<div class="service-list--text">
						<h3>Укладка рулонного газона</h3>
						<p>Профессиональная укладка рулонного газона. 100% гарантия приживаемости. Срок выполнения работ от 1 дня.</p>
						<div class="show-more-link"><a href="">Подробнее</a></div>
					</div>
					<div class="service-list--price">
						от 49 руб./кв.м. <br><small>Цены указаны с НДС 20%</small>
					</div>
				</div>
			</div>
			<?include "inc/trust-block.php"?>
			<?include "inc/benefits-block.php"?>		
		</div>
	</div>

	<?include "inc/question-form.php"?>
	<?include "inc/article-determined-block.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>