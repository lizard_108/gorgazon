<?php
/**
 * Template Name: страница "Отзывы - 1"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	

		<h1>Отзывы</h1>

		<div class="reviews-top-place">
			<div class="text">
				<p>Узнайте почему более <strong>2000 человек</strong> воспользовались услугами и продукцией Горгазона</p>
				<div class="btn-green js-add-review">Оставить отзыв</div>
			</div>
			<div class="rating-place">
				<div class="count">4.5</div>
				<div class="stars-and-text">
					<div class="module-stars size-middle" data-rating="4.5">
						<div class="module-stars--control"></div>
					</div>
					<small>665 + отзывов</small>
				</div>
			</div>
		</div>

		<ul class="tab-switcher">
			<li data-open="1" class="on">Все</li>
			<li data-open="2">О газоне</li>
			<li data-open="3">О создании газонов</li>
			<li data-open="4">Об уходе за газоном</li>
			<li data-open="5">О других товарах и услугах</li>
		</ul>
		
		<div class="main-content">
			<div class="hidden-sections">
				<?
				for ($d=0; $d < 5; $d++) { ?>
				<div class="section">
					<div class="reviews">
						<div class="reviews--list">
							<?
							for ($i=0; $i < 6; $i++) { ?>
							<div class="reviews--item">
								<div class="reviews--avatar" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/avatar-demo.jpg)"></div>
								<div class="reviews--text-place">
									<div class="reviews--link">Отзыв о услуге: <a href="">Укладка рулонного газона на готовое основание</a></div>
									<div class="reviews-total">
										<div class="number-rating">4.5</div>
										<div class="stars"><img src="<?=get_stylesheet_directory_uri()?>/images/other/rating.png" alt=""></div>
									</div>
									<div class="reviews--people-info">
										<span>Игорь</span>
										<span>Организация</span>
									</div>
									<div class="reviews--review-text">Разнообразный и богатый опыт реализация намеченных плановых заданий способствует подготовки и реализации существенных финансовых и административных условий. Идейные соображения высшего порядка, а также постоянное информационно-пропагандистское обеспечение нашей деятельности позволяет оценить значение модели развития. Таким образом постоянный количественный рост и сфера нашей активности требуют определения и уточнения дальнейших направлений развития.</div>
									<div class="reviews--attach-list">
										<img src="<?=get_stylesheet_directory_uri()?>/images/other/reviews-attach.jpg">
										<img src="<?=get_stylesheet_directory_uri()?>/images/other/reviews-attach.jpg">
										<img src="<?=get_stylesheet_directory_uri()?>/images/other/reviews-attach.jpg">
									</div>
								</div>
							</div>
							<?}?>
						</div>
						<div class="blog-controls">
							<div class="blog-controls--show-more">Загрузить еще</div>
							<ul class="blog-controls--pagination">
								<li>1</li>
								<li><a href="">2</a></li>
								<li><a href="">3</a></li>
							</ul>
						</div>
					</div>
				</div>
				<?}?>
			</div>

			<div class="space"></div>

			<?include "inc/trust-block.php"?>
		</div>
	</div>

	<?include "inc/question-form.php"?>
	<?include "inc/article-determined-block.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>