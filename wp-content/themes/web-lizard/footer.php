<?php
/**
 * Шаблон подвала (footer.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
//wp_nav_menu("menu=main-menu");
?>
			<footer>
				<div class="footer--level">
					<div class="group">
						<div class="cell size-33 ">
							<h4 class="js-toggle-ul">Разделы каталога</h4>
							<ul>
								<li><a href="">Рулонный газон</a></li>
								<li><a href="">Семена газонных трав</a></li>
								<li><a href="">Посадочный материал</a></li>
								<li><a href="">Тротуарная плитка</a></li>
								<li><a href="">Газонный грунт</a></li>
								<li><a href="">Удобрения для газона</a></li>
							</ul>
						</div>
						<div class="cell size-33">
							<h4 class="js-toggle-ul">Услуги</h4>
							<ul>
								<li><a href="">Укладка рулонного газона</a></li>
								<li><a href="">Посев газона</a></li>
								<li><a href="">Уход за газоном</a></li>
								<li><a href="">Выезд озеленителя</a></li>
								<li><a href="">Ландшафтный дизайн</a></li>
								<li><a href="">Мощение дорожек</a></li>
							</ul>
						</div>
						<div class="cell size-33 false-block">
							<h4>&nbsp;</h4>
							<ul>
								<li><a href="">Автополив</a></li>
								<li><a href="">Водоотведение</a></li>
								<li><a href="">Посадка растений</a></li>
								<li><a href="">Землянные работы</a></li>
								<li><a href="">Создание цветников и огородов</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="footer--level two-level">
					<div class="group align-center">
						<div class="cell size-33">
							<div class="social">
								Мы в соцсетях: 
								<a href="#"><i class="icons-Facebook"></i></a>
								<a href="#"><i class="icons-VK"></i></a>
								<a href="#"><i class="icons-instagram"></i></a>
							</div>
						</div>
						<div class="cell size-33">
							<a href="">Скачать прайс лист</a>
						</div>
						<div class="cell size-33">
							<a href="">Карточка предприятия </a>								
						</div>
					</div>
				</div>
				<div class="footer--level last-level">
					<div class="group align-center">
						<div class="cell size-66">
							Рулонный газон от компании Горгазон. Работать с нами просто!
						</div>
						<div class="cell size-33">
							<a href="">Политика обработки персональных данных</a>
						</div>
					</div>
				</div>
			</footer>
		</div> <!-- end .main -->
		<!-- /// -->
	</div>
	<!-- end .main-container -->
</div>
<?php wp_footer(); // необходимо для работы плагинов и функционала  ?>
</body>
</html>