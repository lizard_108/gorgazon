<?php
/**
 * Template Name: страница "База знаний 1"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	
		<div class="main-content">
			<h1 class="no-caps">База знаний</h1>
			<p class="page-text">Актуальная информация касательно выбора, содержания и эксплуатационных особенностей разных видов газона</p>
			<div class="page-search">
				<input type="text" placeholder="Поиск">
				<div class="submit"></div>
			</div>
			<ul class="article-list">
				<li>
					<a href="">Газон под ключ</a>
					<ul>
						<li><a href="">Оборудование “John Deere” при производстве рулонного газона</a></li>
						<li>
							<a href="">Низкорослые газонные травы</a>
							<ul>
								<li><a href="">Рулонный газон</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="">Газон Канада грин</a></li>
				<li><a href="">Газон Лилипут для ленивых</a></li>
				<li><a href="">Что такое спортивный газон?</a></li>
				<li>
					<a href="">Английский газон</a>
					<ul>
						<li><a href="">Создание газонов — технологии, виды трав, подготовка участка</a></li>
						<li><a href="">Посадка газона</a></li>
					</ul>
				</li>
				<li><a href="">Укладка рулонного газона на склоне</a></li>
				<li>
					<a href="">Обслуживание газона</a>
					<ul>
						<li>
							<a href="">Устройство газона</a>
							<ul>
								<li><a href="">Технология укладки рулонного газона</a></li>
								<li><a href="">Вертикуляция</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="">Теневой газон — пытаемся разобраться</a></li>
				<li><a href="">Устройство клумб и газонов на участке</a></li>
				<li><a href="">Покрытие для газона — мятлик многолетний</a></li>
				<li><a href="">Газонная трава райграс (или райграсс)</a></li>
				<li><a href="">Секрет наилучшей лужайки – земля для газона!</a></li>
				<li>
					<a href="">Декоративный газон — методы посадки</a>
					<ul>
						<li>
							<a href="">Оздоровление почвы для газона</a>
							<ul>
								<li><a href="">Выбираем триммер</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="">Вся польза профессиональной укладки рулонного газона</a></li>
				<li><a href="">Газон на крыше</a></li>
				<li>
					<a href="">Как правильно стричь газон?</a>
					<ul>
						<li><a href="">Как не ошибиться, выбирая газонокосилку</a></li>
						<li><a href="">Газон в саду</a></li>
						<li><a href="">Идеальный газон</a></li>
						<li><a href="">Автоматический полив</a></li>
					</ul>
				</li>
				<li><a href="">Правильный полив газона</a></li>
				<li>
					<a href="">Граница газона</a>
					<ul>
						<li><a href="">Посадка газонной травы</a></li>
						<li>
							<a href="">Уход за газоном после зимы</a>
							<ul>
								<li><a href="">Газонная дилемма: какой газон выбрать – рулонный или посевной?</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="">Вредители газона: эффективные методы борьбы</a></li>
			</ul>
			<div class="space"></div>
			<div class="index-banner">
				<img src="<?=get_stylesheet_directory_uri()?>/images/other/index-banner.png">
			</div>
			<h2 class="h2-link">Блог <a href="">все статьи</a></h2>

			<div class="blog-list adapt-overflow">
				<?
				for ($i=0; $i < 2; $i++) { ?>
				<div class="blog-list-item">
					<div class="blog-list-item--image" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/blog-item.png)"></div>
					<div class="blog-list-item--place">
						<div class="rating"><img src="<?=get_stylesheet_directory_uri()?>/images/other/rating.png" alt=""></div>
						<div class="title">Правильная укладка рулонного газона</div>
						<p>Универсальный газон, подходит для широкого спектра озеленения</p>
						<ul class="blog-attributes">
							<li><a href="">Укладка</a></li>
							<li>12 декабря 2019</li>
							<li>Горгазон</li>
						</ul>
					</div>
				</div>
				<?}?>
			</div>
		</div>
	</div>
	
	<?include "inc/question-form.php"?>
	<?include "inc/article-determined-block.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>