<?php
/**
 * Страница 404 ошибки (404.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // Подключаем header.php ?>

<? include 'inc/inner-subheader.php';?>

<div class="page-inner page-default">
	<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
	<div class="content">
		<div class="container">
			<div class="grid">
				<div class="cell-3">
					<? include 'inc/left-side.php';?>
				</div>
				<div class="cell-9">
					<h1>Ошибка 404</h1>
					<p>Такой страницы не существует</p>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); // подключаем footer.php ?>