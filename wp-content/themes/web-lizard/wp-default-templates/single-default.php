<script>
	$(document).ready(function() {
		$(".content-place a:has(img)").fancybox();
	});
</script>

<div class="page-inner page-default page-single">
	<div class="content">
		<div class="container">
			<div class="grid">
				<? include 'inc/left-side.php'; ?>
				<div class="cell-9">
					<h1><?php the_title(); ?></h1>
					<div class="content-place">
						<?php if ( have_posts() ) while ( have_posts() ) : the_post(); // старт цикла ?>
							<?
							$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'your_thumb_handle' );
							$thumbnail = $thumbnail['0'];
							if ($thumbnail != "") {?>
								<img src="<?=$thumbnail?>" alt="<?php the_title();?>" class="page-image">
							<?}?>
							<?php the_content(); // контент ?>
						<?php endwhile; // конец цикла ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


