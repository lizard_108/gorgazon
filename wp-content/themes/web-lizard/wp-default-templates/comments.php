<?php
/**
 * Шаблон комментариев (comments.php)
 * Выводит список комментариев и форму добавления
 * @package WordPress
 * @subpackage your-clean-template
 */
?>
<div class="comments"> 
	<div class="coments-count">Комментарии: <?php echo get_comments_number(); // общие кол-во комментов ?></div>
	<?php if (have_comments()) : // если комменты есть ?>
	<?php if (comments_open()) { 
		/* ФОРМА КОММЕНТИРОВАНИЯ */
		$fields =  array( 
			// разметка текстовых полей формы
			'author' => '<label for="author"><input placeholder="Имя" id="author" name="author" type="text" value="'.esc_attr($commenter['comment_author']).'" size="30" required></label>', // поле Имя
			'email' => '<label for="email"><input placeholder="Email" id="email" name="email" type="text" value="'.esc_attr($commenter['comment_author_email']).'" size="30" required></label>', // поле email
			);
		$args = array( // опции формы комментирования
			'fields' => apply_filters('comment_form_default_fields', $fields), // заменяем стандартные поля на поля из массива выше ($fields)
			'comment_field' => '<label for="comment"><textarea id="comment" name="comment" cols="45" rows="8" placeholder="Комментарий"></textarea></label>', // разметка поля для комментирования
			'id_form' => 'commentform', // атрибут id формы
			'id_submit' => 'submit', // атрибут id кнопки отправить
			'title_reply' => 'Оставить отзыв', // заголовок формы
			'title_reply_to' => 'Ответить %s', // "Ответить" текст
			'cancel_reply_link' => 'Отменить ответ', // "Отменить ответ" текст
			'label_submit' => 'Отправить' // Текст на кнопке отправить
		);

		/* Следующий кусок кода будет менять разметку формы, которую мы не можем изменить стандартным функционалом wp */
		/* Например, это может понадобиться, если надо сделать форму на бутстрапе */
		ob_start(); // включаем буферизацию вывода
		
		echo '<div class="comment-form">';
	    comment_form($args); // показываем нашу форму
		echo '</div>';

	    $what_changes = array( // массив с заменой эдементов, ключ - то, что меняем. значение - то, на что меняем
    		'<small>' => '', // удалим <small> тэг
    		'</small>' => '', // удалим <small> тэг
    		// '<h3 id="reply-title" class="comment-reply-title">' => '<span id="reply-title">', // заменим h3 на span
    		// '</h3>' => '</span>', // заменим h3 на span
    		'<input name="submit" type="submit" id="submit" class="btn pink" value="'.$args['label_submit'].'" />' => '<button type="submit" class="btn red" >'.$args['label_submit'].'</button>' // заменим submit input на button
    	);
	    $new_form = str_replace(array_keys($what_changes), array_values($what_changes), ob_get_contents()); // меняем элементы в форме
	    ob_end_clean(); // очищаем буферизацию
	    echo $new_form; // выводим новую форму
	} ?>
	<ul class="comment-list">
		<?php
			$args = array( // аргументы для списка комментариев, некоторые опции выставляются в админке, остальное в классе clean_comments_constructor
				'walker' => new clean_comments_constructor, // класс, который собирает все структуру комментов, нах-ся в function.php
			);
			wp_list_comments($args); // выводим комменты
		?>
	</ul>
	<?php if (get_comment_pages_count() > 1 && get_option( 'page_comments')) : // если страниц с комментами > 1 и пагинация комментариев включена ?>
	<?php $args = array( // аргументы для пагинации
			'prev_text' => '«', // текст назад
			'next_text' => '»' // текст вперед
		); 
		paginate_comments_links($args); // выводим пагинацию
	?>
	<? endif; ?>
	<? endif; ?>
	
</div>