<?php
/**
 * Запись в цикле (loop.php)
 * @package WordPress
 * @subpackage your-clean-template
 */ 
?>
	

	<div class="loop-item">
		<div><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
		
		<p><a href="<?php the_permalink(); ?>"><?php the_content('');?></a></p>
	</div>