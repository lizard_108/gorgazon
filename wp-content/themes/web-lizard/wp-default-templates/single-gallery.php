
<div class="page-inner page-default page-single">
	<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
	<div class="content">
		<div class="container">
			<div class="grid">
				<? include 'inc/left-side.php';?>
				<div class="cell-9">
					<h1><?php the_title(); ?></h1>
					
					<?
					$gallery = get_field("gallery");
					
					?>

					<div class="gallery-slider">
						<?
						foreach ($gallery as $item) {?>
							<div class="item" style="background-image: url(<?=$item['sizes']['large']?>)"></div>							
						<?}?>
					</div>

					<h2>Другие наши работы</h2>

					<? include 'inc/inner-gallery.php'; ?>
				</div>
			</div>
		</div>
	</div>
</div>

