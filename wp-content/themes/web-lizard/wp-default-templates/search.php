<?php
/**
 * Шаблон поиска (search.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 


<div class="page-inner page-default page-single">
	<div class="content">
		<div class="container">
			<div class="grid">
				<? include 'inc/left-side.php'; ?>
				<div class="cell-9">
					<h1><?php printf('Поиск по строке: %s', get_search_query());  ?></h1>
					<div class="content-place">
						<div class="group">
						<?php if (have_posts()) : while (have_posts()) : the_post();?>
							<div class="cell size-100" style="margin-bottom: 1rem;">
								<div><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
								<?php the_content('');?>
							</div>
						<?php endwhile; // конец цикла
						else: echo '<h2>Нет записей.</h2>'; endif; ?>	 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




<div class="page-inner page-default page-search">
	<div class="content">
		<div class="container">
			<div class="grid">
				<div class="cell-8">
					<h1><?php printf('Поиск по строке: %s', get_search_query());  ?></h1>
					<div class="group">
					<?php if (have_posts()) : while (have_posts()) : the_post();?>
						<div class="cell size-100" style="margin-bottom: 1rem;">
							<div><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
							<?php the_content('');?>
						</div>
					<?php endwhile; // конец цикла
					else: echo '<h2>Нет записей.</h2>'; endif; ?>	 
					</div>
				</div>
				<? include 'inc/right-side.php';?>
			</div>
		</div>
	</div>
</div>


<?php get_footer();  ?>


