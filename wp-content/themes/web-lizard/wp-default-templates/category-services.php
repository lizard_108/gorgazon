<?php
/**
 * Шаблон рубрики (category.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<?

$currentCat = get_category(get_query_var('cat'));

?>

<div class="page-inner page-default page-category">
	<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
	<div class="content">
		<div class="container">
			<div class="grid">
				<? include 'inc/left-side.php';?>
				<div class="cell-9">
					<h1><?=$currentCat->cat_name?></h1>
					<div class="services-list">
						<div class="group">
							<div class="cell size-33">
								<img src="/project/images/other/ser-1.png">
								<div class="a">Замена всех типов покрытий, ремонт систем СКС, ГГС, АПС, СДИ и т.д.</div>
								<div class="b"><a href="">Обслуживание и ремонт</a></div>
							</div>
							<div class="cell size-33">
								<img src="/project/images/other/ser-2.png">
								<div class="a">Замена элементов внешнего оформления объекта</div>
								<div class="b"><a href="">Замена облицовочных панелей</a></div>
							</div>
							<div class="cell size-33">
								<img src="/project/images/other/ser-3.png">
								<div class="a">Ремонт островка из брусчатки с каймой из нержавеющей стали</div>
								<div class="b"><a href="">Ремонт островка ТРК</a></div>
							</div>
							<div class="cell size-33">
								<img src="/project/images/other/ser-4.png">
								<div class="a">По периметру подсветка led, а может быть и люминисцентная</div>
								<div class="b"><a href="">Ремонт подсветки фриза</a></div>
							</div>
							<div class="cell size-33">
								<img src="/project/images/other/ser-5.png">
								<div class="a">Обслуживание и ремон внешнего вида и подсветки табло</div>
								<div class="b"><a href="">Ремонт стеллы, ценовое табло</a></div>
							</div>
							<div class="cell size-33">
								<img src="/project/images/other/ser-6.png">
								<div class="a">Установка новых или замена знаков въезд, выезд</div>
								<div class="b"><a href="">Ремонт навигации АЗС</a></div>
							</div>
						</div>
					</div>

					<h2>Качественный ремонт и обслуживание</h2>
					<p style="font-size: 18px; font-weight: 300; line-height: 2em;">На сегодняшний день большая нагрузка на оборудование создает потребность в регулярном ремонте и обслуживании оборудования на АЗС и нефтебазах. Для обеспечения работоспособности и предотвращения быстрого износа предлагаем Вам услуги по проведению гарантийного, послегарантийного технического обслуживания и ремонта оборудования на АЗС и нефтебазах. Наличие основных узлов и деталей  в сервисном центре позволит быстро устранить любые неисправности оборудования на Вашей АЗС. Ремонт оборудования наши специалисты  производят на объекте, а  при необходимости в нашем сервисном центре.</p>
					<?
					/*
					?>
					<div class="group list">
						<?php if (have_posts()) : while (have_posts()) : the_post(); // если посты есть - запускаем цикл wp ?>
						<?
							
						$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'your_thumb_handle' );
						$thumbnail = $thumbnail['0'];

						$dateEnd = get_field("date", $post->ID);
						if ($thumbnail != "") {

						} else {
							$thumbnail = "/project/images/other/no-photo.jpg";
						}
						?>
						<div class="cell size-100">
							<div class="row">
								<div class="img" style="background-image: url(<?=$thumbnail?>);"><a href="<?php the_permalink() ?>"></a></div>						
							</div>
							<div class="row">
								<div class="title"><a href="<?php the_permalink() ?>"><?php the_title()?></a></div>
								<div class="description">
									<a href="<?php the_permalink() ?>"><?php the_excerpt();?></a>
								</div>								
							</div>
						</div>
						<?php endwhile; // конец цикла
						else: echo '<h2>Нет записей.</h2>'; endif; // если записей нет, напишим "простите" ?>	 
					</div>
					<?*/?>
					<div class="pagination">
						<?php pagination(); // пагинация, функция нах-ся в function.php ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); // подключаем footer.php ?>