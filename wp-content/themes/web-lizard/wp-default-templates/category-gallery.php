<?php
/**
 * Шаблон рубрики (category.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<?

$currentCat = get_category(get_query_var('cat'));

?>

<div class="page-inner page-default page-category">
	<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
	<div class="content">
		<div class="container">
			<div class="grid">
				<? include 'inc/left-side.php';?>
				<div class="cell-9">
					<h1><?=$currentCat->cat_name?></h1>
					<?
					$args = array(
						'numberposts' => -1,
						'category'    => 4,
						'orderby'     => 'date',
						'order'       => 'DESC',
					);

					$gallery_list = get_posts($args);

					?>

					<div class="inner-gallery">
						<div class="group">
							<?
							foreach ($gallery_list as $item) {
								
								$post_gal = get_field("gallery", $item->ID);
								$gl_count = 0;
								$main_img = "";
								foreach ($post_gal as $gal) {
									$gl_count+=1;
									if ($gl_count == 1) {
										$main_img = $gal['sizes']['large'];
									}
								}
							?>
								<div class="cell size-33">
									<div class="level-0" style="background-image: url(<?=$main_img?>);">
										<div class="count">
											<div><?=$gl_count?></div>
											<div>фото</div>
										</div>
										<a href="<?the_permalink($item->ID);?>"></a>
									</div>
									<div class="level-1">
										<a href="<?the_permalink($item->ID);?>"><?=$item->post_title?></a>
									</div>
								</div>			
							<?}?>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); // подключаем footer.php ?>