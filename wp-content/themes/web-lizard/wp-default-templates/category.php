<?php
/**
 * Шаблон рубрики (category.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<?

$currentCat = get_category(get_query_var('cat'));

?>

<div class="page-inner page-default page-category">
	<div class="content">
		<div class="container">
			<div class="grid">
				<div class="cell-8">
					<h1><?=$currentCat->cat_name?></h1>
					
					<div class="interesting">
						<div class="group">
							<?php if (have_posts()) : while (have_posts()) : the_post(); // если посты есть - запускаем цикл wp ?>
							<?
								
							$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'your_thumb_handle' );
							$thumbnail = $thumbnail['0'];

							$dateEnd = get_field("date", $post->ID);
							if ($thumbnail != "") {

							} else {
								$thumbnail = "/project/images/other/no-photo.jpg";
							}
							?>
							<div class="cell size-50">
								<div class="image" style="background-image: url(<?=$thumbnail?>"><a href="<?php the_permalink() ?>"></a></div>
								<div class="place">
									<div class="title"><a href="<?php the_permalink() ?>"><?php the_title()?></a></div>
									<div class="descr"><a href="<?php the_permalink() ?>"><?php the_excerpt();?></a></div>
								</div>							
							</div>							
							<?php endwhile; // конец цикла
							else: echo '<h2>Нет записей.</h2>'; endif; // если записей нет, напишим "простите" ?>	 
						</div>
					</div>
					
					

				
					<div class="pagination">
						<?php pagination(); // пагинация, функция нах-ся в function.php ?>
					</div>
				</div>
				<? include 'inc/right-side.php';?>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); // подключаем footer.php ?>