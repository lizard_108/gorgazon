<?php
/**
 * Шаблон отдельной записи (single.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php 

// для сбора информации просмотра страницы
setPostViews(get_the_ID()); 
$cat = get_the_category($post->ID);

switch ($cat[0]->slug) {
	case 'gallery':
		include_once 'single-gallery.php';
		break;
	default:
		include_once 'single-default.php';
		break;
}
?>

<?php get_footer(); // подключаем footer.php ?>
