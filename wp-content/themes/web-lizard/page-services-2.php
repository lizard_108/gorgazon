<?php
/**
 * Template Name: страница "Услуги - 2 - укладка рулонного газона"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	

		<div class="services-submenu">
			<div class="services-submenu--title">Услуги</div>


<ul id="menu-services" class="services-submenu--menu"><li id="menu-item-277" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-40 current_page_item menu-item-277"><a href="https://gorsad.ru/uslugi/ukladka-rulonnogo-gazona/" aria-current="page">Укладка рулонного газона</a></li>
<li id="menu-item-279" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-279"><a href="https://gorsad.ru/uslugi/posevnoy-gazon/">Посевной газон</a></li>
<li id="menu-item-278" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-278"><a href="https://gorsad.ru/uslugi/uhod-za-gazonom/">Уход за газоном</a></li>
<li id="menu-item-280" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-280"><a href="https://gorsad.ru/uslugi/vyezd-ozelenitelya/">Выезд озеленителя</a></li>
<li id="menu-item-288" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-288"><a href="#">Еще</a>
<ul class="sub-menu">
	<li id="menu-item-281" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-281"><a href="https://gorsad.ru/uslugi/landshaftnyy-dizayn/">Ландшафтный дизайн</a></li>
	<li id="menu-item-282" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-282"><a href="https://gorsad.ru/uslugi/moschenie-dorozhek/">Мощение дорожек</a></li>
	<li id="menu-item-283" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-283"><a href="https://gorsad.ru/uslugi/avtopoliv/">Автополив</a></li>
	<li id="menu-item-284" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-284"><a href="https://gorsad.ru/uslugi/vodootvedenie/">Водоотведение</a></li>
	<li id="menu-item-285" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-285"><a href="https://gorsad.ru/uslugi/posadka-rasteniy/">Посадка растений</a></li>
	<li id="menu-item-286" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-286"><a href="https://gorsad.ru/uslugi/zemlyanye-raboty/">Земляные работы</a></li>
	<li id="menu-item-287" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-287"><a href="https://gorsad.ru/uslugi/sozdanie-tsvetnikov-i-ogorodov/">Создание цветников и огородов</a></li>
</ul>
</li>
</ul>








			<!-- <ul class="services-submenu--menu">
				<li class="current"><a href="">Укладка рулонного газона</a></li>
				<li><a href="">Посев газона</a></li>
				<li><a href="">Уход за газоном</a></li>
				<li><a href="">Выезд озеленителя</a></li>
				<li><a href="">Ландшафтный дизайн</a></li>
				<li class="last"><a href="">Ещё</a></li>
			</ul> -->
		</div>
		
		<div class="main-content">
			<h1>Укладка рулонного газона</h1>
			<div class="btn-green content-button js-open-callback-form">Оставить заявку</div>
			<div class="space"></div>
			<?for ($d=0; $d < 4; $d++) { ?>
			<h2>На готовое основание <?=$d?></h2>
			<div class="services-list-2">
				<?for ($i=0; $i < 2; $i++) { ?>
				<div class="services-list-2--item">
					<div class="services-list-2--img" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/demo-2.png)"></div>
					<div class="services-list-2--text">
						<h3>Укладка рулонного газона на готовое основание</h3>
						<p>Универсальный газон, подходит для широкого спектра озеленения</p>
						<div class="flex services-product-rating">
							<?
							include 'inc/stars.php';
							?>
							<div class="services-list-2--reviews">66 отзывов</div>						
						</div>
						<div class="services-list-2--price">от 149 руб./кв.м.</div>
						<div class="group">
							<div class="cell size-50">Цены указаны с НДС 20%</div>
							<div class="cell size-50"><div class="show-more-link"><a href="">Подробнее</a></div></div>
						</div>
					</div>
				</div>
				<?}?>
			</div>
			<?}?>

			<?include "inc/trust-block.php"?>
			<?include "inc/benefits-block.php"?>	
			<?include "inc/green-block.php"?>	
			<?include "inc/price-table.php"?>	
			
			<!-- /// -->
			<h2 class="reviews-title">Отзывы <span>о укладке рулонного газона</span></h2>
			<div class="reviews">
				<div class="reviews-total">
					<div class="number-rating">4.5</div>
					<div class="big-stars"><img src="<?=get_stylesheet_directory_uri()?>/images/other/big-stars.png" alt=""></div>
				</div>
				<div class="total-reviews-count">66 отзывов</div>
				<div class="reviews--list reviews-list-in-categoty">
					<?
					for ($i=0; $i < 3; $i++) { ?>
					<div class="reviews--item">
						<div class="reviews--avatar" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/avatar-demo.jpg)"></div>
						<div class="reviews--text-place">
							<div class="reviews--link">Отзыв о услуге: <a href="">Укладка рулонного газона на готовое основание</a></div>
							<div class="reviews-total">
								<div class="number-rating">4.5</div>
								<div class="stars"><img src="<?=get_stylesheet_directory_uri()?>/images/other/rating.png" alt=""></div>
							</div>
							<div class="reviews--people-info">
								<span>Игорь</span>
								<span>Организация</span>
							</div>
							<div class="reviews--review-text">Разнообразный и богатый опыт реализация намеченных плановых заданий способствует подготовки и реализации существенных финансовых и административных условий. Идейные соображения высшего порядка, а также постоянное информационно-пропагандистское обеспечение нашей деятельности позволяет оценить значение модели развития. Таким образом постоянный количественный рост и сфера нашей активности требуют определения и уточнения дальнейших направлений развития.</div>
						</div>
					</div>
					<?}?>
				</div>
				<div class="show-more-button">
					<a href="">Показать больше</a>
				</div>
			</div>
			<!-- /// -->
			<div class="calc-banner">
				<h3>Калькулятор стоимости</h3>
				<p>Проведите предварительный расчет стоимости</p>
				<div class="show-more-link"><a href="">Рассчитать</a></div>
			</div>

			<div class="delevery-banner">
				<h3>Доставка</h3>
				<p>Обратите внимание на условия доставки рулонного газона</p>
				<div class="show-more-link"><a href="">Подробнее</a></div>
			</div>

			<?include "inc/garanty-and-pay-blocks.php"?>
			
		</div>
	</div>

	<?include "inc/question-form.php"?>
	<?include "inc/article-determined-block.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>