<?php
/**
 * Функции шаблона (function.php)
 * @package WordPress
 * @subpackage your-clean-template
 */



function typical_title() { // функция вывода тайтла
	global $page, $paged; // переменные пагинации должны быть глобыльными
	wp_title('|', true, 'right'); // вывод стандартного заголовка с разделителем "|"
	bloginfo('name'); // вывод названия сайта
	$site_description = get_bloginfo('description', 'display'); // получаем описание сайта
	if ($site_description && (is_home() || is_front_page())) //если описание сайта есть и мы на главной
		echo " | $site_description"; // выводим описание сайта с "|" разделителем
	if ($paged >= 2 || $page >= 2) // если пагинация была использована
		echo ' | '.sprintf(__( 'Страница %s'), max($paged, $page)); // покажем номер страницы с "|" разделителем
}

register_nav_menus(array( // Регистрируем 2 меню
	'top' => 'Верхнее', // Верхнее
	'bottom' => 'Внизу' // Внизу
));

add_theme_support('post-thumbnails'); // включаем поддержку миниатюр
set_post_thumbnail_size(250, 150); // задаем размер миниатюрам 250x150
add_image_size('big-thumb', 400, 400, true); // добавляем еще один размер картинкам 400x400 с обрезкой

register_sidebar(array( // регистрируем левую колонку, этот кусок можно повторять для добавления новых областей для виджитов
	'name' => 'Колонка слева', // Название в админке
	'id' => "left-sidebar", // идентификатор для вызова в шаблонах
	'description' => 'Обычная колонка в сайдбаре', // Описалово в админке
	'before_widget' => '<div id="%1$s" class="widget %2$s">', // разметка до вывода каждого виджета
	'after_widget' => "</div>\n", // разметка после вывода каждого виджета
	'before_title' => '<span class="widgettitle">', //  разметка до вывода заголовка виджета
	'after_title' => "</span>\n", //  разметка после вывода заголовка виджета
));

class clean_comments_constructor extends Walker_Comment { // класс, который собирает всю структуру комментов
	public function start_lvl( &$output, $depth = 0, $args = array()) { // что выводим перед дочерними комментариями
		$output .= '<ul class="children">' . "\n";
	}
	public function end_lvl( &$output, $depth = 0, $args = array()) { // что выводим после дочерних комментариев
		$output .= "</ul><!-- .children -->\n";
	}
    protected function comment( $comment, $depth, $args ) { // разметка каждого комментария, без закрывающего </li>!
    	$classes = implode(' ', get_comment_class()).($comment->comment_author_email == get_the_author_meta('email') ? ' author-comment' : ''); // берем стандартные классы комментария и если коммент пренадлежит автору поста добавляем класс author-comment
        echo '<li id="li-comment-'.get_comment_ID().'" class="'.$classes.'">'."\n"; // родительский тэг комментария с классами выше и уникальным id
    	echo '<div id="comment-'.get_comment_ID().'">'."\n"; // элемент с таким id нужен для якорных ссылок на коммент
    	echo get_avatar($comment, 64)."\n"; // покажем аватар с размером 64х64
    	echo '<p class="meta">Автор: '.get_comment_author()."\n"; // имя автора коммента
    	echo ' '.get_comment_author_email(); // email автора коммента
    	echo ' '.get_comment_author_url(); // url автора коммента
    	echo ' Добавлено '.get_comment_date('F j, Y').' в '.get_comment_time()."\n"; // дата и время комментирования
    	if ( '0' == $comment->comment_approved ) echo '<em class="comment-awaiting-moderation">Ваш комментарий будет опубликован после проверки модератором.</em>'."\n"; // если комментарий должен пройти проверку
        comment_text()."\n"; // текст коммента
        $reply_link_args = array( // опции ссылки "ответить"
        	'depth' => $depth, // текущая вложенность
        	'reply_text' => 'Ответить', // текст
			'login_text' => 'Вы должны быть залогинены' // текст если юзер должен залогинеться
        );
        echo get_comment_reply_link(array_merge($args, $reply_link_args)); // выводим ссылку ответить
        echo '</div>'."\n"; // закрываем див
    }
    public function end_el( &$output, $comment, $depth = 0, $args = array() ) { // конец каждого коммента
		$output .= "</li><!-- #comment-## -->\n";
	}
}

function pagination() { // функция вывода пагинации
	global $wp_query; // текущая выборка должна быть глобальной
	$big = 999999999; // число для замены
	echo paginate_links(array( // вывод пагинации с опциями ниже
		'base' => str_replace($big,'%#%',esc_url(get_pagenum_link($big))), // что заменяем в формате ниже
		'format' => '?paged=%#%', // формат, %#% будет заменено
		'current' => max(1, get_query_var('paged')), // текущая страница, 1, если $_GET['page'] не определено
		'type' => 'list', // ссылки в ul
		'prev_text'    => 'Назад', // текст назад
    	'next_text'    => 'Вперед', // текст вперед
		'total' => $wp_query->max_num_pages, // общие кол-во страниц в пагинации
		'show_all'     => false, // не показывать ссылки на все страницы, иначе end_size и mid_size будут проигнорированны
		'end_size'     => 15, //  сколько страниц показать в начале и конце списка (12 ... 4 ... 89)
		'mid_size'     => 15, // сколько страниц показать вокруг текущей страницы (... 123 5 678 ...).
		'add_args'     => false, // массив GET параметров для добавления в ссылку страницы
		'add_fragment' => '',	// строка для добавления в конец ссылки на страницу
		'before_page_number' => '', // строка перед цифрой
		'after_page_number' => '' // строка после цифры
	));
}

/**
 * Создание файлов для отправке поп почте с уникаленым токеном
 *
 */
function upload_attachment(){
	$upload_dir = ABSPATH.'wp-content/uploads/attachment/';
	$count = count($_FILES['files']['name']);
	for($i=0;$i<$count;$i++){
		$upload_file = $upload_dir .$_POST['token'].'_'.basename($_FILES['files']['name'][$i]);
		if (move_uploaded_file($_FILES['files']['tmp_name'][$i], $upload_file)) {
			echo "Файл корректен и был успешно загружен.\n";
		} else {
			echo "Возможная атака с помощью файловой загрузки!\n";
		}
	}
	die();
}
add_action("wp_ajax_nopriv_upload_attachment", "upload_attachment");
add_action("wp_ajax_upload_attachment", "upload_attachment");

function prefix_pre_get_posts($query) {
	if ($query->is_category) {
		$query->set('post_type', 'any');
	}
	return $query;
}
 
add_action('pre_get_posts', 'prefix_pre_get_posts');


// ***********

// Создаем тип записей Movies 
/*register_post_type('movies', array(
	'label' => 'Услуги',
	'public' => true,
	'show_ui' => true,
	'capability_type' => 'post',
	'hierarchical' => false,
	'rewrite' => array('slug' => 'services'),
	'query_var' => true,
	'supports' => array(
		'title',
		'editor',
		'excerpt',
		'trackbacks',
		'custom-fields',
		'comments',
		'revisions',
		'thumbnail',
		'author',
		'page-attributes')
	)
);*/

function collect() {
	// ********** получение списка всех постов и создание массива с ACF полями
	$args = array(
		'category'		  => 6,
		'numberposts'     => -1,  
		'orderby'         => 'post_date',
		'order'           => 'DESC',
		'post_type'       => 'post',
		'post_status'     => 'publish'
	);
	$posts = get_posts($args); 

	$allTypes = array();
	foreach ($posts as $post) {
		$type = get_field("type", $post->ID);
		$price = get_field("price", $post->ID);
		$term = get_field("term", $post->ID);
		$model = get_field("model", $post->ID);
		$link = get_permalink($post->ID);
		// print_r($type);
		if ($type) {
			$currentArray = array(
				"id" => $post->ID, 
				"type" => $type, 
				"price" => $price,
				"term" => $term,
				"model" => $model,
				"link" => $link
			);
			array_push($allTypes, $currentArray);
		} else {
			// echo $type."__хуй<br>";
		}
		// $allTypes = array_unique($allTypes);
	}
	return $allTypes;
}

function get_device_menue($id) {
	$m_args = array(
		'hide_empty' => 0,
		'child_of' => $id,
		'number' => '0'
	);
	$menue_array = get_categories($m_args);
	foreach ($menue_array as $menue_item) {
		$m_link = get_category_link($menue_item->term_id);
		echo '<li><a href="'.$m_link.'">'.$menue_item->name.'</a></li>';
	}
}

function new_excerpt_length($length) {
	return 8;
}
add_filter('excerpt_length', 'new_excerpt_length');

function currentUrl() {
	echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
}

function get_works() {
	$array = array();

	$offset = $_POST['offset'];
	

	$args = array(
		'category'		  => 3,
		'numberposts'     => 2,  
		'offset' => $offset,
		'orderby'         => 'post_date',
		'order'           => 'DESC',
		'post_type'       => 'post',
		'post_status'     => 'publish'
	);

	$works_list = get_posts($args);
	foreach ($works_list as $work) {
		$before = get_field('before', $work->ID);
		$after = get_field('after', $work->ID);	

		$img_before = $before['sizes']['large'];
		$img_before_full = $before['url'];

		$img_after = $after['sizes']['large'];
		$img_after_full = $after['url'];

		$push = array(
			'before' => $img_before,
			'before_full' => $img_before_full,
			'after' => $img_after,
			'after_full' => $img_after_full,
			'offset' => $offset,
		);
		array_push($array, $push);
	}



	echo json_encode($array);
	die();
}

function dimox_breadcrumbs() {

  /* === ОПЦИИ === */
  $text['home'] = 'Главная'; // текст ссылки "Главная"
  $text['category'] = '%s'; // текст для страницы рубрики
  $text['search'] = '%s'; // текст для страницы с результатами поиска
  $text['tag'] = '%s'; // текст для страницы тега
  $text['author'] = '%s'; // текст для страницы автора
  $text['404'] = 'Ошибка 404'; // текст для страницы 404
  $text['page'] = '%s'; // текст 'Страница N'
  $text['cpage'] = '%s'; // текст 'Страница комментариев N'

  $wrap_before = '<div class="breadcrumbs"><div class="container">'; // открывающий тег обертки
  $wrap_after = '</div></div><!-- .breadcrumbs -->'; // закрывающий тег обертки
  $sep = '›'; // разделитель между "крошками"
  $sep_before = '<span class="sep">'; // тег перед разделителем
  $sep_after = '</span>'; // тег после разделителя
  $show_home_link = 1; // 1 - показывать ссылку "Главная", 0 - не показывать
  $show_on_home = 0; // 1 - показывать "хлебные крошки" на главной странице, 0 - не показывать
  $show_current = 1; // 1 - показывать название текущей страницы, 0 - не показывать
  $before = '<span class="current">'; // тег перед текущей "крошкой"
  $after = '</span>'; // тег после текущей "крошки"
  /* === КОНЕЦ ОПЦИЙ === */

  global $post;
  $home_link = home_url('/');
  $link_before = '<span itemscope itemtype="Breadcrumb">';
  $link_after = '</span>';
  $link_attr = ' itemprop="url"';
  $link_in_before = '';
  $link_in_after = '';
  $link = $link_before . '<a href="%1$s"' . $link_attr . '>' . $link_in_before . '%2$s' . $link_in_after . '</a>' . $link_after;
  $frontpage_id = get_option('page_on_front');
  $parent_id = $post->post_parent;
  // $sep = ' ' . $sep_before . $sep . $sep_after . ' ';
  $sep = "";

  if (is_home() || is_front_page()) {

    if ($show_on_home) echo $wrap_before . '<a href="' . $home_link . '">' . $text['home'] . '</a>' . $wrap_after;

  } else {
    echo $wrap_before;
    if ($show_home_link) echo sprintf($link, $home_link, $text['home']);

    
    if ( is_category() ) {
      $cat = get_category(get_query_var('cat'), false);
      if ($cat->parent != 0) {
        $cats = get_category_parents($cat->parent, TRUE, $sep);
        $cats = explode("</span>", $cats);
        if (count($cats) > 3) {
          if ($show_home_link) echo $sep;
        }
        $cats = implode("</span>", $cats);
        $cats = str_replace('<a href="http://apple-sapphire.ru/category/uslugi/">Все</a> <span class="sep">›</span> <a href="http://apple-sapphire.ru/category/uslugi/device/">Девайсы</a> <span class="sep">›</span> ', '', $cats);
        $cats = str_replace('<a href="http://apple-sapphire.ru/category/uslugi/">Все</a> <span class="sep">›</span> <a href="http://apple-sapphire.ru/category/uslugi/failures/">Поломки</a> <span class="sep">›</span> ', '', $cats);
        $cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
        $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);

        // 
        // var_dump($cats);

        $cats = str_replace('<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://apple-sapphire.ru/category/uslugi/" itemprop="url"><span itemprop="title">Все</span></a></span> <span class="sep">›</span> <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://apple-sapphire.ru/category/uslugi/device/" itemprop="url"><span itemprop="title">Девайсы</span></a></span> <span class="sep">›</span>', '', $cats);
        $cats = str_replace('<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://apple-sapphire.ru/category/uslugi/" itemprop="url"><span itemprop="title">Все</span></a></span> <span class="sep">›</span> <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://apple-sapphire.ru/category/uslugi/device/" itemprop="url"><span itemprop="title">Девайсы</span></a></span>', '', $cats);        

        // 

        echo $cats;



        // ____________



      }
      if ( get_query_var('paged') ) {
        $cat = $cat->cat_ID;
        echo $sep . sprintf($link, get_category_link($cat), get_cat_name($cat)) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
      } else {
        // if ($show_current) echo $sep . $before . sprintf($text['category'], single_cat_title('', false)) . $after;
        if ($show_current) echo $sep . $before . sprintf($text['category'], single_cat_title('', false)) . $after;
      }

    } elseif ( is_search() ) {
      if (have_posts()) {
        if ($show_home_link && $show_current) echo $sep;
        if ($show_current) echo $before . sprintf($text['search'], get_search_query()) . $after;
      } else {
        if ($show_home_link) echo $sep;
        echo $before . sprintf($text['search'], get_search_query()) . $after;
      }

    } elseif ( is_day() ) {
      if ($show_home_link) echo $sep;
      echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $sep;
      echo sprintf($link, get_month_link(get_the_time('Y'), get_the_time('m')), get_the_time('F'));
      if ($show_current) echo $sep . $before . get_the_time('d') . $after;

    } elseif ( is_month() ) {
      if ($show_home_link) echo $sep;
      echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y'));
      if ($show_current) echo $sep . $before . get_the_time('F') . $after;

    } elseif ( is_year() ) {
      if ($show_home_link && $show_current) echo $sep;
      if ($show_current) echo $before . get_the_time('Y') . $after;

    } elseif ( is_single() && !is_attachment() ) {
      if ($show_home_link) echo $sep;
      if ( get_post_type() != 'post' ) {
        
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        printf($link, $home_link . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
        if ($show_current) echo $sep . $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); 
        $cat = $cat[0];
        $cats = get_category_parents($cat, TRUE, $sep);

        // 

        $cats = str_replace('<a href="http://apple-sapphire.ru/category/uslugi/">Все</a> <span class="sep">›</span> <a href="http://apple-sapphire.ru/category/uslugi/device/">Девайсы</a> <span class="sep">›</span> ', '', $cats);

        // 


        if (!$show_current || get_query_var('cpage')) $cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
        $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
        echo $cats;
        if ( get_query_var('cpage') ) {
          echo $sep . sprintf($link, get_permalink(), get_the_title()) . $sep . $before . sprintf($text['cpage'], get_query_var('cpage')) . $after;
        } else {
          if ($show_current) echo $before . get_the_title() . $after;
        }
      }

    // custom post type
    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      if ( get_query_var('paged') ) {
        echo $sep . sprintf($link, get_post_type_archive_link($post_type->name), $post_type->label) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
      } else {
        if ($show_current) echo $sep . $before . $post_type->label . $after;
      }

    } elseif ( is_attachment() ) {
      if ($show_home_link) echo $sep;
      $parent = get_post($parent_id);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      if ($cat) {
        $cats = get_category_parents($cat, TRUE, $sep);
        $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
        echo $cats;
      }
      printf($link, get_permalink($parent), $parent->post_title);
      if ($show_current) echo $sep . $before . get_the_title() . $after;

    } elseif ( is_page() && !$parent_id ) {
      if ($show_current) echo $sep . $before . get_the_title() . $after;

    } elseif ( is_page() && $parent_id ) {
      if ($show_home_link) echo $sep;
      if ($parent_id != $frontpage_id) {
        $breadcrumbs = array();
        while ($parent_id) {
          $page = get_page($parent_id);
          if ($parent_id != $frontpage_id) {
            $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
          }
          $parent_id = $page->post_parent;
        }
        $breadcrumbs = array_reverse($breadcrumbs);
        for ($i = 0; $i < count($breadcrumbs); $i++) {
          echo $breadcrumbs[$i];
          if ($i != count($breadcrumbs)-1) echo $sep;
        }
      }
      if ($show_current) echo $sep . $before . get_the_title() . $after;

    } elseif ( is_tag() ) {
      if ( get_query_var('paged') ) {
        $tag_id = get_queried_object_id();
        $tag = get_tag($tag_id);
        echo $sep . sprintf($link, get_tag_link($tag_id), $tag->name) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
      } else {
        if ($show_current) echo $sep . $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
      }

    } elseif ( is_author() ) {
      global $author;
      $author = get_userdata($author);
      if ( get_query_var('paged') ) {
        if ($show_home_link) echo $sep;
        echo sprintf($link, get_author_posts_url($author->ID), $author->display_name) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
      } else {
        if ($show_home_link && $show_current) echo $sep;
        if ($show_current) echo $before . sprintf($text['author'], $author->display_name) . $after;
      }

    } elseif ( is_404() ) {
      if ($show_home_link && $show_current) echo $sep;
      if ($show_current) echo $before . $text['404'] . $after;

    } elseif ( has_post_format() && !is_singular() ) {
      if ($show_home_link) echo $sep;
      echo get_post_format_string( get_post_format() );
    }

    echo $wrap_after;

  }
} // end of dimox_breadcrumbs()


/* количество просмотров */
 
 function getPostViews($postID){
 $count_key = 'post_views_count';
 $count = get_post_meta($postID, $count_key, true);
 if($count==''){
 delete_post_meta($postID, $count_key);
 add_post_meta($postID, $count_key, '0');
 return "0 просмотров";
 }
 return ' Просмотров: '.$count;
 }
 function setPostViews($postID) {
 $count_key = 'post_views_count';
 $count = get_post_meta($postID, $count_key, true);
 if($count==''){
 $count = 0;
 delete_post_meta($postID, $count_key);
 add_post_meta($postID, $count_key, '0');
 }else{
 $count++;
 update_post_meta($postID, $count_key, $count);
 }
 }


function get_gg_extend_menu(){

    $html = "<div class='services-submenu'>
        <div class='services-submenu--title'>Услуги</div>" .

            wp_nav_menu( [
                'menu'  => 'Services',
                'container' => '',
                'menu_class' => 'services-submenu--menu',
                'echo'      => false
            ] ) .

        "</div>";
}

function get_gg_rating_html( $rating, $count, $size_class ) {

    $starimg_url = get_stylesheet_directory_uri() . '/images/other/big-stars.png';
    $revtext = get_gg_revtext( $count );

    $html = "<div class='reviews-total {$size_class}'>
                <div class='number-rating'>{$rating}</div>
                <div class='big-stars'><img src='{$starimg_url}' alt=''></div>
            </div>
            <div class='total-reviews-count'>{$revtext}</div>";
    return $html;
}

function get_gg_revtext( $cnt ) {
    if ( $cnt && count( $cnt ) > 0 ) {
        $last_dig = (int) substr( $cnt, - 1 );
        if ( $last_dig == 1 ) {
            return "( {$cnt} отзыв )";
        } elseif ( $last_dig < 5 && $last_dig > 1 ) {
            return "( {$cnt} отзыва )";
        } else {
            return "( {$cnt} отзывов )";
        }
    } else {
        return 'нет отзывов';
    }
}