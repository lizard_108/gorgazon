<?php
/**
 * Template Name: страница "Оплата"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	
		<div class="main-content">
			<h1>Оплата</h1>
			<div class="maintext">
				<p>Рулонный газон <strong>не подлежит складскому хранению</strong>. Газон срезается на производстве <strong>индивидуально под каждого клиента</strong>. Поэтому мы работаем на условиях <strong>100% предоплаты</strong>. Оплата доставки возможна по факту и обсуждается индивидуально.</p>
				<p>Мы работаем как с юридическими лицами, так и с физическими лицами.</p>
			</div>
			<br>
			<h3>Безналичный расчет</h3>
			<p>Мы работаем с НДС 20%. Этот способ оплаты возможен как с юридическими, так и с физическими лицами. Оплата производится в соответствии с договором купли-продажи на расчетный счет организации.</p>
			<br>
			<h3>Наличный расчет</h3>
			<p>Оплата производится в офисе организации.</p>
		</div>
	</div>
	
	<?include "inc/question-form.php"?>
	<?include "inc/article-determined-block.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>