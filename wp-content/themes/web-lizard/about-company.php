<?php
/**
 * Template Name: страница "О компании"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	

		
		<div class="main-content">
			<h1>О компании Горгазон</h1>

			<p>У нас Вы можете купить <b>рулонный газон</b>, а также заказать устройство <b>посевного газона</b>. А еще мы предлагаем недорогую и оперативную <b>доставку газона</b> по европейской части России. Мы принципиально не зарабатываем на доставке нашего газона.</p>
		</div>
	</div>
	<div class="about-company-prevelegios">
		<h2 class="no-caps">Бесспорными преимуществами газонов Горгазон являются:</h2>
		<div class="group">
			<div class="cell size-50">
				<img src="<?=get_stylesheet_directory_uri()?>/images/svg/about-1.svg">
				<h3>Контроль качества</h3>
				<p>Продажа газонов осуществляется только после контроля качества, мы не можем себе позволить опорочить свою репутацию! Если вы купили наши газоны, то можете смело рассчитывать на превосходный результат использования такого покрытия.</p>
			</div>
			<div class="cell size-50">
				<img src="<?=get_stylesheet_directory_uri()?>/images/svg/about-2.svg">
				<h3>Собственные производственные мощности</h3>
				<p>Это материально – техническая база предприятия: специально обрабатываемые поля, на которых выращивается готовый газон только высшего качества; спецтехника по посеву, уходу и срезу готового материала; продуманная система орошения.</p>
			</div>
			<div class="cell size-50">
				<img src="<?=get_stylesheet_directory_uri()?>/images/svg/about-3.svg">
				<h3>Надежные поставщики</h3>
				<p>Наши поставщики, в свою очередь, это отлично зарекомендовавшиеся на мировом рынке производители семян Jacklin Seeds, Seed Research, известные не только в США, но и за ее пределами.</p>
			</div>
			<div class="cell size-50">
				<img src="<?=get_stylesheet_directory_uri()?>/images/svg/about-4.svg">
				<h3>Международные стандарты</h3>
				<p>Процесс производства газонов сертифицирован по международным стандартам ISO 9000 и ISO 14000, что немаловажно для соответствия запросам самых требовательных клиентов России, стремящихся к поистине мировым нормативам.</p>
			</div>
			<div class="cell size-50">
				<img src="<?=get_stylesheet_directory_uri()?>/images/svg/about-5.svg">
				<h3>Обработка заказов</h3>
				<p>Оперативная обработка заказов не допустит проволочек и задержек — мы очень ценим ваше время, поэтому готовы работать на протяжении немыслимо увеличенного рабочего дня: с 9 до 23 часов!</p>
			</div>
			<div class="cell size-50">
				<img src="<?=get_stylesheet_directory_uri()?>/images/svg/about-6.svg">
				<h3>Передовые технологии производства </h3>
				<p>Делая упор на специализированном современном оборудовании, наша компания беспроигрышно опережает конкурентов.</p>
			</div>
		</div>
	</div>
	<div class="content-container">
		<div class="space"></div>
		<div class="about-company-numbers">
			<h2 class="no-caps">Цифры</h2>
			<div class="group">
				<div class="cell size-50">
					<h2>2 250 000 м2</h2>
					<p>Площадь выращенного газонного покрытия высочайшего европейского качества.</p>
				</div>
				<div class="cell size-50">
					<h2>16 000</h2>
					<p>Завершонных проектов</p>
				</div>
				<div class="cell size-50">
					<h2>150</h2>
					<p>Опытных сотрудников в штате</p>
				</div>
				<div class="cell size-50">
					<h2>16 </h2>
					<p>Лет работы на рынке</p>
				</div>
			</div>
		</div>
		<div class="space"></div>
		<?include "inc/realize-projects.php"?>
		<div class="space"></div>
		<?include "inc/trust-block.php"?>
		<h2 class="h2-link">Отзывы о работе <a href="">все отзывы</a></h2>
		<div class="reviews">
			<div class="reviews--list reviews--slider">
				<?
				for ($i=0; $i < 3; $i++) { ?>
					<div class="item">
						<div class="reviews--item">
							<div class="reviews--avatar" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/avatar-demo.jpg)"></div>
							<div class="reviews--text-place">
								<div class="reviews--link">Отзыв о услуге: <a href="">Укладка рулонного газона на готовое основание</a></div>
								<div class="reviews-total">
									<div class="number-rating">4.5</div>
									<div class="stars"><img src="<?=get_stylesheet_directory_uri()?>/images/other/rating.png" alt=""></div>
								</div>
								<div class="reviews--people-info">
									<span>Игорь</span>
									<span>Организация</span>
								</div>
								<div class="reviews--review-text">Разнообразный и богатый опыт реализация намеченных плановых заданий способствует подготовки и реализации существенных финансовых и административных условий. Идейные соображения высшего порядка, а также постоянное информационно-пропагандистское обеспечение нашей деятельности позволяет оценить значение модели развития. Таким образом постоянный количественный рост и сфера нашей активности требуют определения и уточнения дальнейших направлений развития.</div>
							</div>
						</div>
					</div>
				<?}?>					
			</div>
		</div>
	</div>

	<?include "inc/question-form.php"?>
	<?include "inc/article-determined-block.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>