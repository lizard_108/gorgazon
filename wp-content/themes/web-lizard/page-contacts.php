<?php
/**
 * Template Name: страница "Контакты"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	

		
		<div class="main-content">
			<h1 class="h2-link">Контакты <a href="">Карточка предприятия</a></h1>
			<div class="contacts-page">
				<div class="group">
					<div class="cell size-50">
						<h3>Обратившись в компанию «Горгазон», вы выигрываете дважды:</h3>
					</div>
					<div class="cell size-50">
						<ul class="line-list">
							<li>первоклассный рулонный газон по честной цене;</li>
							<li>оперативная  и дешевая доставка рулонного газона распространяется на любую точку европейской части России — мы принципиально не зарабатываем на доставке!</li>
						</ul>
					</div>
				</div>
				<div class="space"></div>
				<div class="group contacts-info">
					<div class="cell size-50">
						<h3>Телефон</h3>
						<br>
						<br>
						<h3>+7 (495) 281-88-36</h3>
						<p>для Московского региона</p>
						<br>
						<h3>8-800 505-07-36</h3>
						<p>для других регионов России</p>
						<br>
						<br>
						<div class="btn-green">Связаться с нами</div>
					</div>
					<div class="cell size-50">
						<h3>Email</h3>
						<br><br>
						<h3>sales@gorgazon.ru</h3>
						<p>по коммерческим вопросам</p>
						<br>
						<h3>info@gorgazon.ru</h3>
						<p>по общим вопросам не связанным с коммерцией</p>
					</div>
				</div>
			</div>
			<div class="space"></div>
			<h3 class="no-caps">Мы в соц. сетях</h3>
			<div class="contacts-social">
				<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-facebook.svg"></a>
				<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-vk.svg"></a>
				<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-instagram.svg"></a>
			</div>
			<div class="space"></div>
			<h3 class="no-caps">Офис продаж</h3>
			<h3 class="no-caps">Россия, г. Москва, Б. Толмачёвский пер., дом 5, стр. 1 (м. Третьяковская)</h3>
			<p><small>На территории действует пропускная система. Просьба заказывать пропуск заранее</small></p>
			<div class="map">
				<div class="map-button">Вызвать такси</div>
				<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aad67bc39c32788326ba60ba735e587a99ac6c03c648eb94e8650ee9d27d69c15&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
			</div>
			<div class="space"></div>
			<h3>Производственная база</h3>
			<h3>Россия, г. Москва, МКАД, 15-й километр, вл. 2А</h3>
			<div class="map">
				<div class="map-button">Вызвать такси</div>
				<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ac80c0beef3d6143e97cf79f2cc5c67d9796952ba507b557b5f7b2fa255ba7314&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
			</div>
		</div>
	</div>

	
	<?include "inc/question-form.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>