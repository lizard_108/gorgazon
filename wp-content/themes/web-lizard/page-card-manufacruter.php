<?php
/**
 * Template Name: страница "Карточка предприятия"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	
		
		<div class="main-content">
			<h1>Карточка предприятия ООО «АГОРА ГР»</h1>
			<p><b>Юридический адрес: </b>
			<br>
			125475, г. Москва, Петрозаводская ул, д. 28, корп. 4, пом VI, ком 2</p>
			
			<p><b>ОГРН:</b><br>5157746119592</p>
			
			<p><b>ОКПО:</b><br>52407892</p>
			
			<p><b>ОКВЭД:</b><br>01.12.2</p>
			
			<p><b>ИНН:</b><br>7743130556</p>
			
			<p><b>КПП:</b><br>774301001</p>
			
			<p><b>Наименование банка:</b><br>АО «АЛЬФА-БАНК»</p>
			
			<p><b>БИК:</b><br>044525593</p>
			
			<p><b>Кор/сч:</b><br>30101810200000000593</p>
			
			<p><b>Расчетный счет:</b><br>40702810402740001399</p>
			
			<p><b>Генеральный директор:</b><br>Крючков Руслан Сергеевич</p>
		</div>
	</div>

	<?include "inc/question-form.php"?>
</main>
<? get_footer(); // подключаем footer.php ?>