<?php
/**
 * Template Name: страница "Выезд специалиста по озеленению  и благоустройству"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	

		
		<div class="main-content">
			<h1>Выезд специалиста по озеленению  и благоустройству</h1>
			<p>Для долговечного озеленения необходимо правильно произвести подготовку основания. Это является гарантией, что озеленение будет долговечным. В нашей компании работают дипломированные специалисты различных областей озеленения. Это позволяет решать любую задачу в озеленении на самом высоком профессиональном уровне.</p>
			<div class="big-green-button onmain">Заказать выезд озеленителя</div>
			<div class="text-padding-place">
				<h3 class="no-caps">На вашем объекте специалиста по озеленению  и благоустройству:</h3>
				<ul class="line-list">
					<li>определит задачи предстоящего озеленения и благоустройства;</li>
					<li>произведет необходимые замеры (площадь благоустройства, анализ почвы, анализ территории и т.д.);</li>
					<li>определит перечень и объем необходимых работ;</li>
					<li>предложит варианты оптимизации затрат;</li>
					<li>даст консультации по вопросам озеленения и благоустройства.</li>
				</ul>
				<p>По результатам осмотра будет подготовлен сметный расчет работ по озеленению и благоустройству.</p>
			</div>
			<div class="space"></div>
			<div class="index-banner">
				<img src="<?=get_stylesheet_directory_uri()?>/images/other/index-banner.png">
			</div>
			<?include "inc/trust-block.php"?>
		</div>
	</div>

	<?include "inc/question-form.php"?>
	<?include "inc/article-determined-block.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>