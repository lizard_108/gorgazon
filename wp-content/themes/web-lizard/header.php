<?php
/**
 * Шаблон шапки (header.php)
 * @package WordPress
 * @subpackage your-clean-template
 */


?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="<?php bloginfo( 'charset' ); // кодировка ?>">
	<?php /* RSS и всякое */ ?>
	<link rel="alternate" type="application/rdf+xml" title="RDF mapping" href="<?php bloginfo('rdf_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="RSS" href="<?php bloginfo('rss_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="Comments RSS" href="<?php bloginfo('comments_rss2_url'); ?>">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/style.css">
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<link rel="stylesheet" href="<?=get_stylesheet_directory_uri()?>/reject/reject.css" media="all" />
		<script type="text/javascript" src="<?=get_stylesheet_directory_uri()?>/reject/reject.min.js"></script>
	<![endif]-->
	<title><?php typical_title();  ?></title>
	<?php wp_head(); ?>


	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	

	<link rel="stylesheet" href="<?=get_stylesheet_directory_uri()?>/css/style.css?<?=time();?>">
	<link rel="stylesheet" href="<?=get_stylesheet_directory_uri()?>/js/lib/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="<?=get_stylesheet_directory_uri()?>/js/lib/owl-carousel/owl.transitions.css">
	<script src="<?=get_stylesheet_directory_uri()?>/js/lib/jquery-1.12.4.min.js"></script>
	<script src="<?=get_stylesheet_directory_uri()?>/js/lib/owl-carousel/owl.carousel.min.js"></script>
	<script src="<?=get_stylesheet_directory_uri()?>/js/lib/jquery.formstyler.min.js"></script>
	<script src="<?=get_stylesheet_directory_uri()?>/js/sliders.js"></script>
	<script src="<?=get_stylesheet_directory_uri()?>/js/custom.js"></script>
</head>
<body>
	<div class="window-blur"></div>
	<?
	include 'inc/modal-window-calc.php';
	include 'inc/modal-window-add-review.php';
	include 'inc/modal-window-callback.php';
	?>
	<div class="wrapper">
		<a href="tel:84991367886" class="adapt--mobile-button"></a>
		<div class="adapt-header">
			<div class="adapt-header--wr">
				<div class="adapt-header--sandwich js-open-sandwich"></div>
				<div class="adapt-header--logo"></div>
				<div class="adapt-header--buttons">
					<a href="" class="button-search js-open-adapt-search"></a>
					<a href="" class="button-share js-open-adapt-share"></a>
					<a href="" class="button-calc js-open-calc"></a>
				</div>
			</div>
			<div class="sandwich-open">
				<div class="top-level">
					<div class="adapt-header--logo"></div>
					<div class="adapt-header--close js-close-sandwich"></div>
				</div>
				<div class="menu-level">
					<div class="header--actions-discounts">
						<a href="">Акции и скидки</a>
					</div>
					<nav>
						<ul class="header--menu part-1">
							<li><a href="">Продукция</a></li>
							<li><a href="">Услуги</a></li>
							<li><a href="">О компании</a></li>
							<li><a href="">Проекты</a></li>
							<li><a href="">Отзывы</a></li>
							<li><a href="">Блог</a></li>
							<li><a href="">Контакты</a></li>
						</ul>
						<ul class="header--menu part-2">
							<li class="has-child">
								<a href="">Покупателю</a>
								<ul>
									<li><a href="">Вопрос ответ</a></li>
									<li><a href="">База знаний</a></li>
									<li><a href="">Доставка</a></li>
									<li><a href="">Оплата</a></li>
									<li><a href="">Гарантия</a></li>
								</ul>
							</li>
							<li><a href="">Сотрудничество</a></li>
							<li><a href="">Вакансии</a></li>
						</ul>
					</nav>
					<div class="adapt-header--contacts">
						<div class="adapt-header--phone">
							<div><strong>8 (499) 136-78-86</strong></div>
							<div>Москва</div>
						</div>
						<div class="adapt-header--phone">
							<div><strong>8 (800) 770-76-86</strong></div>
							<div>Россия</div>
						</div>
						<div class="text-1">ежедневно c 9:00 до 23:00 </div>
						<div class="text-2">sales@gorgazon.ru</div>
					</div>
				</div>
			</div>
			<div class="adapt-search-open">
				<div class="search">
					<form action="">
						<input type="text" placeholder="Поиск по сайту" value="" name="search" class="search--input">
					</form>
				</div>
			</div>
			<div class="adapt-share-open">
				<div class="adapt-share-open--wr">
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-facebook.svg"></a>
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-vk.svg"></a>
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-instagram.svg"></a>
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-od.svg"></a>
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/svg/social-email.svg"></a>
				</div>
			</div>
		</div>
		<div class="main-container">
			<header>
				<div class="main-logo">
					<a href="/">
						<img src="<?=get_stylesheet_directory_uri()?>/images/svg/main-logo.svg" alt="Горгазон">
						<br>
						<span>Горгазон</span>
					</a>
				</div>
				<div class="header--phone">
					<div class="current">8 (499) 136-78-86</div>
					<div class="get-callback">
						<a href="">Заказать обратный звонок</a>
					</div>
				</div>
				<div class="header--actions-discounts">
					<a href="">Акции и скидки</a>
				</div>
				<nav>
					<ul class="header--menu">
						<li><a href="">Продукция</a></li>
						<li><a href="">Услуги</a></li>
						<li><a href="">О компании</a></li>
						<li><a href="">Проекты</a></li>
						<li><a href="">Отзывы</a></li>
						<li><a href="">Блог</a></li>
						<li><a href="">Контакты</a></li>
					</ul>
					<ul class="header--menu part-2">
						<li class="has-child">
							<a href="">Покупателю</a>
							<ul>
								<li><a href="">Вопрос ответ</a></li>
								<li><a href="">База знаний</a></li>
								<li><a href="">Доставка</a></li>
								<li><a href="">Оплата</a></li>
								<li><a href="">Гарантия</a></li>
							</ul>
						</li>
						<li><a href="">Сотрудничество</a></li>
						<li><a href="">Вакансии</a></li>
					</ul>
				</nav>
				<div class="header--email">
					sales@gorgazon.ru
				</div>
				<div class="header-banner">
					<a href="/"><img src="<?=get_stylesheet_directory_uri()?>/images/other/header-banner.jpg"></a>
				</div>
			</header>
			<div class="main">