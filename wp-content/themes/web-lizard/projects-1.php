<?php
/**
 * Template Name: страница "Проекты 1"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	
		<div class="main-content">
			<h1 class="no-caps">Реализованые проекты</h1>
			<div class="maintext">
				<p>За прошедший сезон 2018 года, коллективом компании Горгазон было реализовано <strong>более 300 различных проектов</strong> озеленения и благоустройства в Москве и Московской области. </p>
			</div>
			

			<div class="group realize-projects-list">
				<?
				for ($i=0; $i < 12; $i++) { ?>
				<div class="cell size-50">
					<div class="image" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/project-demo.png)"></div>
					<ul class="blog-attributes">
						<li>Укладка</li>
						<li><a href="">85 м<sup>2</sup></a></li>
					</ul>
					<div class="title">Озеленение рулонным газоном крыши БЦ Москва</div>
				</div>
				<?}?>
			</div>
			<div class="blog-controls">
				<div class="blog-controls--show-more">Загрузить еще</div>
				<ul class="blog-controls--pagination">
					<li>1</li>
					<li><a href="">2</a></li>
					<li><a href="">3</a></li>
				</ul>
			</div>
		</div>
	</div>
	
	<?include "inc/question-form.php"?>
	<?include "inc/article-determined-block.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>