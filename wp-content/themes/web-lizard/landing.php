<?php
/**
 * Template Name: страница "Лэндинг"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	

		
		<div class="main-content">
			<div class="landing-page">
				<h1>Посадка растений</h1>
				<div class="subtitle">Для долговечного озеленения необходимо правильно произвести подготовку основания. </div>
				<div class="button">
					<a href="" class="btn-green">Перейти на сайт</a>
				</div>
				<div class="image"><img src="<?=get_stylesheet_directory_uri()?>/images/other/landing.jpg" alt=""></div>
			</div>
			<?include "inc/trust-block.php"?>
			<div class="landing-page--articles">
				<div class="group align-center">
					<div class="cell size-50 text-place">
						<h3 class="no-caps">Кем и как определяется стоимость устройства газона</h3>
						<p>В отличие от остальных компаний, которые для упрощения своей деятельности заведомо завышают цены на свои услуги до максимума, компания “Горгазон” исповедует совсем другой подход.</p>
						<p>Стоимость укладки рулонного газона в Москве и области (до 50 км от МКАД) определяет профессиональный озеленитель, выезд которого бесплатен. Специалист по благоустройству и озеленению произведет все необходимые замеры, с целью минимизировать затраты клиента на укладку рулонного газона.</p>
					</div>
					<div class="cell size-50 image-place">
						<img src="<?=get_stylesheet_directory_uri()?>/images/other/demo-5.jpg">
					</div>
				</div>
				<div class="group align-center">
					<div class="cell size-50 image-place">
						<img src="<?=get_stylesheet_directory_uri()?>/images/other/demo-5.jpg">
					</div>
					<div class="cell size-50 text-place">
						<h3 class="no-caps">Кем и как определяется стоимость устройства газона</h3>
						<p>В отличие от остальных компаний, которые для упрощения своей деятельности заведомо завышают цены на свои услуги до максимума, компания “Горгазон” исповедует совсем другой подход.</p>
						<p>Стоимость укладки рулонного газона в Москве и области (до 50 км от МКАД) определяет профессиональный озеленитель, выезд которого бесплатен. Специалист по благоустройству и озеленению произведет все необходимые замеры, с целью минимизировать затраты клиента на укладку рулонного газона.</p>
					</div>
				</div>
				<div class="group align-center ">
					<div class="cell size-50 text-place">
						<h3 class="no-caps">Кем и как определяется стоимость устройства газона</h3>
						<p>В отличие от остальных компаний, которые для упрощения своей деятельности заведомо завышают цены на свои услуги до максимума, компания “Горгазон” исповедует совсем другой подход.</p>
						<p>Стоимость укладки рулонного газона в Москве и области (до 50 км от МКАД) определяет профессиональный озеленитель, выезд которого бесплатен. Специалист по благоустройству и озеленению произведет все необходимые замеры, с целью минимизировать затраты клиента на укладку рулонного газона.</p>
					</div>
					<div class="cell size-50 image-place">
						<img src="<?=get_stylesheet_directory_uri()?>/images/other/demo-5.jpg">
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<? get_footer(); // подключаем footer.php ?>