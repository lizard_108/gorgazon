<?php
/**
 * Template Name: страница "Акции 2"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	
		<div class="main-content">
			
			<div class="actions-page">
				<div class="actions-page--image">
					<img src="<?=get_stylesheet_directory_uri()?>/images/other/index-banner.png">
				</div>
				<div class="actions-page--date">
					до 05 октября 2019
				</div>
				<h1 class="actions-page--title">Каждый третий поддон с рулонным газон бесплатно!</h1>
				<p>При заказе рулонного газона</p>
				<p>– Стандарт, <br>
				 – Первый сорт,   <br>
				– Люкс,   <br>
				– Тень Люкс   <br>
				– Спортивный <br><br>
				каждый третий поддон газона – бесплатно!</p>
			</div>

			<div class="space"></div>
		</div>
	</div>
	
	<?include "inc/question-form.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>