<?php
/**
 * Template Name: страница "База знаний - 2 "
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	
		<div class="main-content">
			<h1 class="no-caps">Газон под ключ</h1>
			<ul class="blog-attributes on-blog-page">
				<li>
					<div class="reviews-total">
						<div class="number-rating">4.5</div>
						<?
						include 'inc/stars.php';
						?>
					</div>
				</li>
			</ul>
			<div class="space"></div>
			<div class="blog-page-inner">
				<div class="blog-page-inner--image">
					<img src="<?=get_stylesheet_directory_uri()?>/images/other/blog-1.jpg">
				</div>
				<div class="space"></div>
				<div class="blog-page-inner--text">
					<h3>Как формируется стоимость газона под ключ</h3>
					<p>Стоимость газона под ключ рассчитывается исходя из стоимости маткриала (рулонов газона) и работ, необходимых для завершения формирования непосредственно самого газона.</p>
					<p>Если со стоимостью рулонного газона все предельно ясно: цену на них всегда можно посмотреть в прайс-листах, то с работой все не так однозначно. Даже для сходных по площади участков цена на их озеленение может существенно разниться.</p>
					<p>Происходить это может по множеству причин. Например, сложная геометрия участка. Вполне очевидно, что прямые линии, отсутствие неровностей рельефа территории, предназначенной для озеленения, существенно уменьшают трудозатраты на ее озеленение и, соответственно, ее стоимость.</p>
					<p>Стоимость работ по устройству газона на участках с высотными перепадами, со множеством каких-либо элементов (клумб, дорожек, построек и т.д.), сложной геометрией закономерно возрастает.</p>
				</div>
				<div class="space"></div>
				<div class="blog-page-inner--image">
					<img src="<?=get_stylesheet_directory_uri()?>/images/other/blog-2.jpg">
				</div>
				<div class="space"></div>
				<div class="blog-page-inner--image">
					<a href=""><img src="<?=get_stylesheet_directory_uri()?>/images/other/banner-pallet.jpg"></a>
				</div>
				<div class="space"></div>
				<div class="blog-page-inner--text">
					<p>Стоимость газона под ключ рассчитывается исходя из стоимости маткриала (рулонов газона) и работ, необходимых для завершения формирования непосредственно самого газона.</p>
				</div>
				<div class="space"></div>
				<div class="blog-page-inner--image">
					<img src="<?=get_stylesheet_directory_uri()?>/images/other/blog-4.jpg">
				</div>
				<div class="space"></div>
				<div class="blog-page-inner--image">
					<img src="<?=get_stylesheet_directory_uri()?>/images/other/blog-4.jpg">
				</div>
				<div class="space"></div>
				<div class="blog-page-inner--text">
					<p>Стоимость газона под ключ рассчитывается исходя из стоимости маткриала (рулонов газона) и работ, необходимых для завершения формирования непосредственно самого газона.</p>
					<div class="space"></div>
					<?include 'inc/share-block.php';?>
					<div class="space"></div>

					<h3>Связаные разделы</h3>
					<div class="article-list">
						<li>
							<a href="">Газон под ключ</a>
							<ul>
								<li><a href="">Оборудование “John Deere” при производстве рулонного газона</a></li>
								<li>
									<a href="">Низкорослые газонные травы</a>
									<ul>
										<li><a href="">Рулонный газон</a></li>
									</ul>
								</li>
							</ul>
						</li>
					</div>
					<div class="space"></div>
					<?include 'inc/comments-block.php';?>
					<div class="space"></div>
				</div>
			</div>
			<h2 class="h2-link">Блог <a href="">все статьи</a></h2>
			<div class="blog-list">
				<?
				for ($i=0; $i < 3; $i++) { ?>
				<div class="blog-list-item">
					<div class="blog-list-item--image" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/blog-item.png)"></div>
					<div class="blog-list-item--place">
						<div class="rating"><img src="<?=get_stylesheet_directory_uri()?>/images/other/rating.png" alt=""></div>
						<div class="title">Правильная укладка рулонного газона</div>
						<p>Универсальный газон, подходит для широкого спектра озеленения</p>
						<ul class="blog-attributes">
							<li><a href="">Укладка</a></li>
							<li>12 декабря 2019</li>
							<li>Горгазон</li>
						</ul>
					</div>
				</div>
				<?}?>
			</div>
		</div>
	</div>
	
	<?include "inc/question-form.php"?>
	<?include "inc/article-determined-block.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>