<?php
/**
 * Template Name: страница "Акции"
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 
<main>
	<div class="content-container">
		<?include "inc/search.php"?>
		<?include "inc/breadcrumbs.php"?>	
		<div class="main-content">
			<h1>Акции</h1>
			<div class="actions-list">
				<?
				for ($i=0; $i < 7; $i++) { ?>
				<div class="actions-list--item">
					<a href="">
						<img src="<?=get_stylesheet_directory_uri()?>/images/other/index-banner.png">
						<small>до 05 октября 2019</small>
						<h3>Каждый третий поддон с рулонным газон бесплатно!</h3>
					</a>
				</div>
				<?}?>
			</div>
			<div class="blog-controls">
				<div class="blog-controls--show-more">Загрузить еще</div>
				<ul class="blog-controls--pagination">
					<li>1</li>
					<li><a href="">2</a></li>
					<li><a href="">3</a></li>
				</ul>
			</div>
			<div class="space"></div>
			<h2 class="h2-link">Блог <a href="">все статьи</a></h2>

			<div class="blog-list adapt-overflow">
				<?
				for ($i=0; $i < 2; $i++) { ?>
				<div class="blog-list-item">
					<div class="blog-list-item--image" style="background-image: url(<?=get_stylesheet_directory_uri()?>/images/other/blog-item.png)"></div>
					<div class="blog-list-item--place">
						<div class="rating"><img src="<?=get_stylesheet_directory_uri()?>/images/other/rating.png" alt=""></div>
						<div class="title">Правильная укладка рулонного газона</div>
						<p>Универсальный газон, подходит для широкого спектра озеленения</p>
						<ul class="blog-attributes">
							<li><a href="">Укладка</a></li>
							<li>12 декабря 2019</li>
							<li>Горгазон</li>
						</ul>
					</div>
				</div>
				<?}?>
			</div>
		</div>
	</div>
	
	<?include "inc/question-form.php"?>
	<?include "inc/article-determined-block.php"?>
</main>

<? get_footer(); // подключаем footer.php ?>